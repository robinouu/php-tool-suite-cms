<?php
if( isset($_REQUEST['disconnect']) ){
	session_unset();
	session_destroy();
	unset($_SESSION);
	redirect('/');
}
?>
<div class="col-4">
	<nav>
		<ul>
			<li><a href="/account/profile"><?php print __('MyProfile'); ?></a></li>
			<li><a href="/account/addresses"><?php print __('MyAddresses'); ?></a></li>
<?php if( in_array('eshop', var_get('config/core/plugins')) ) { ?>
			<li><a href="/account/orders"><?php print __('MyOrders'); ?></a></li>
<?php } ?>
			<li><a href="/account/options"><?php print __('MyOptions'); ?></a></li>
			<li><a href="/account/profile?disconnect=1"><?php print __('Disconnection'); ?></a></li>
		</ul>
	</nav>
</div>