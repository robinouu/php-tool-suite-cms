<?php
require_once('header.php');
require_once('account.php');

if( isset($_REQUEST['deleteAddr']) && is_numeric($_REQUEST['deleteAddr'])){
	sql_delete('person_contact', array('where' => 'contact_id='.sql_quote($_REQUEST['deleteAddr'])));
	sql_delete('contact_point', array('where' => 'id='.sql_quote($_REQUEST['deleteAddr'])));
	redirect('/account/addresses');
}

$formAddress = new Form();
$formAddress->btnLabel = __('Save');

$formAddress->children['street_address'] = new TextField(array('name' => 'street_address', 'label' => __('StreetAddress'), 'required' => true));
$formAddress->children['postal_code'] = new TextField(array('name' => 'postal_code', 'label' => __('PostalCode'), 'maxlength' => 10, 'required' => true));
$formAddress->children['address_locality'] = new TextField(array('name' => 'address_locality', 'label' => __('AddressLocality')));
$formAddress->children['address_region'] = new TextField(array('name' => 'address_region', 'label' => __('AddressRegion')));
$formAddress->children['address_country'] = new SelectField(array('name' => 'address_country', 'label' => __('AddressCountry'), 'datas' => get_country_list()));

if( $formAddress->validate($_REQUEST) ){
	if( sql_insert('contact_point', array(
		'street_address' => $_REQUEST['street_address'],
		'postal_code' => $_REQUEST['postal_code'],
		'address_locality' => $_REQUEST['address_locality'],
		'address_region' => $_REQUEST['address_region'],
		'address_country' => $_REQUEST['address_country']
	)) ){
		$contact_id = sql_last_id();
		sql_insert('person_contact', array('contact_id' => $contact_id, 'person_id' => session_var_get('auth/user/person_id')));
	}
}

$addresses = sql_get('contact_point', array(
	'alias' => 'cp',
	'select' => 'cp.*, pc.contact_name',
	'join' => 'INNER JOIN person_contact pc ON pc.contact_id = cp.id',
	'where' => 'pc.person_id='.session_var_get('auth/user/person_id')
));

?>
<div class="col-8">
	<h1><?php print __('MyAddresses'); ?></h1>
	<?php foreach( $addresses as $addr ) { ?>
	<p class="addr">
		<?php print $addr['street_address'] . br() . $addr['postal_code'] . ' ' . $addr['address_locality'] . br() . ltrim($addr['address_region'] . ', ' . $addr['address_country'], ' ,'); ?>
		<a href="/account/addresses?deleteAddr=<?php print $addr['id']; ?>"><?php print __('Delete'); ?></a>
	</p>
	<?php } ?>
	<h2><?php print __('NewAddress'); ?></h2>
	<?php print $formAddress->render(); ?>
</div>

<?php
require_once('footer.php');
?>