<?php

$articles = sql_get('article', array(
	'alias' => 'a',
	'select' => 'a.*, m.url as media_url, c.title as title, c.content as html, p.name as author_name',
	'join' => '
	LEFT JOIN person p ON p.id = a.author
	LEFT JOIN media m ON m.id = a.media
	LEFT JOIN content c ON c.id = a.content'
));

include_once('header.php');

if( !$articles ){
	print p('No article found.');
}else{
	
?>
<h1>Blog entries</h1>
<?php	foreach( $articles as $article ){ ?>
<article class="article">
	<a class="overall" href="post/<?php print $article['id'].'-'.slug($article['title']); ?>.html"></a>
	<h2><?php print htmlspecialchars($article['title']); ?></h2>
	<div class="author">Par <?php print $article['author_name']; ?></div>
	<?php if ($article['media_url']) { ?><figure><img src="<?php print $article['media_url']; ?>" /></figure><?php } ?>
	
	<div class="content">
		<?php print substr(strip_tags($article['html']), 0, 255); ?>
	</div>
</article>
<?php	
	}
}

include_once('footer.php');

?>