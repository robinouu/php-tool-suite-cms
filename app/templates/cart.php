<?php
//session_var_unset('order');
$cart = session_var_get('cart', array());

if( isset($_REQUEST['delete']) && is_numeric($_REQUEST['delete'])){
	foreach( $cart['products'] as $k => $v){
		if( $v['id'] == $_REQUEST['delete']){
			unset($cart['products'][$k]);
		}
	}
	session_var_set('cart', $cart);
	redirect('/cart');
}
if( isset($_REQUEST['deleteDiscount']) && $_REQUEST['deleteDiscount'] == 1){
	unset($cart['discount']);
	session_var_set('cart', $cart);
	redirect('/cart');
}
if( isset($_REQUEST['setDeliveryMethod']) && $_REQUEST['setDeliveryMethod'] == 1){
	unset($cart['delivery']);
	session_var_set('cart', $cart);
	redirect('/cart');	
}

// --- calculate cart price
if( isset($cart['products']) ){
	$cart['price'] = 0;
	foreach( $cart['products'] as $prd ){
		$cart['price'] += $prd['price'] * $prd['qty'];
	}
	if( isset($cart['delivery']) )
		$cart['price'] += $cart['delivery']['shipping_fees'];
	if( isset($cart['discount']) ){
		if( $cart['discount']['price_value'] > 0 ){
			$cart['price'] -= $cart['discount']['price_value'];
		}else if( $cart['discount']['price_percent'] > 0){
			$cart['price'] -= $cart['discount']['price_percent'] * $cart['price'] / 100;
		}
		$cart['price'] = max(0, $cart['price']);
	}
}

// --- main
$formDiscount = new Form();
$formDiscount->name = 'form-discount';
$formDiscount->btnLabel = __('Use');
$formDiscount->children['discount'] = new TextField(array('name' => 'discount', 'label' => __('DiscountCode')));
if( $formDiscount->validate($_REQUEST) ){
	
	$discount = sql_get('discount', array('where' => 'code='.sql_quote($_REQUEST['discount']), 'limit' => 1));
	if( $discount ){
		$dtUntil = new DateTime($discount['apply_until']);
		if( $dtUntil->getTimestamp() >= time() )
			$cart['discount'] = $discount;
		else
			unset($cart['discount']);
	}else{
		unset($cart['discount']);
	}
	session_var_set('cart', $cart);
	redirect('/cart');
}

$formDelivery = new Form();
if( $formDelivery->validate($_REQUEST) ){

	if( isset($_REQUEST['delivery']) ){
		$delivery = sql_get('delivery_method', array('where' => 'id='.(int)$_REQUEST['delivery'], 'limit' => 1));
		if( $delivery ){
			$cart['delivery'] = $delivery;
			session_var_set('cart', $cart);
		}
		redirect('/cart');
	}
}

if( isset($cart['delivery_address'])){
	$addr = sql_get('contact_point', array('where' => 'id='.sql_quote($cart['delivery_address']), 'limit' => 1));
	$all_deliveries = sql_get('delivery_method', array('where' => 'country='.sql_quote($addr['address_country']).' OR country LIKE ""'));
	$all_products_info = array('width' => 0, 'height' => 0, 'depth' => 0, 'weight' => 0);
	if( isset($cart['products']) ){
		foreach( $cart['products'] as $prd ){
			$all_products_info['width'] += (float)$prd['width'];
			$all_products_info['height'] += (float)$prd['height'];
			$all_products_info['depth'] += (float)$prd['depth'];
			$all_products_info['weight'] += (float)$prd['weight'];
		}
	}
	$deliveries = array();
	if( $all_deliveries ){
		foreach( $all_deliveries as $d ){
			if (($d['max_width'] >= $all_products_info['width'] || $d['max_width'] == null) && 
					($d['max_height'] >= $all_products_info['height'] || $d['max_height'] == null) &&
					($d['max_depth'] >= $all_products_info['depth'] || $d['max_depth'] == null) &&
					($d['max_weight'] >= $all_products_info['weight'] || $d['max_weight'] == null) ){
				$deliveries[] = $d;
				$radio = new RadioField(array('name' => 'delivery', 'label' => '<b>'.$d['name'].'</b><br />'.$d['description'], 'value' => $d['id']));
				if( isset($cart['delivery']) && $cart['delivery']['id'] == $d['id'])
					$radio->attributes['checked'] = true;
				$formDelivery->children[] = $radio;
			}
		}
	}
}

$formLogin = new Form();
$formLogin->name = 'login';
$formLogin->btnLabel = __('Connect');
$formLogin->children['email'] = new EmailField(array('name' => 'email', 'label' => __('Email'), 'required' => true));
$formLogin->children['password'] = new PasswordField(array('name' => 'password', 'label' => __('Password'), 'required' => true));
if( $formLogin->validate($_REQUEST) ){
	$user = auth_identify($_REQUEST['email'], $_REQUEST['password']);
	if( $user ){
		session_var_set('auth/user', $user);
	}
	redirect('/cart');
}

$formRegister = new Form();
$formRegister->name = 'register';
$formRegister->btnLabel = __('CreateAccount');
$formRegister->children['gender'] = new SelectField(array('name' => 'gender', 'label' => __('Gender'), 'datas' => array('male' => __('MaleCivility'), 'female' => __('FemaleCivility'))));
$formRegister->children['family_name'] = new TextField(array('name' => 'family_name', 'label' => __('FamilyName'), 'minlength' => 2));
$formRegister->children['given_name'] = new TextField(array('name' => 'given_name', 'label' => __('GivenName'), 'minlength' => 2));
$formRegister->children['email'] = new EmailField(array('name' => 'email', 'label' => __('Email'), 'required' => true));
$formRegister->children['password'] = new PasswordField(array('name' => 'password', 'label' => __('Password'), 'required' => true, 'minlength' => 8));
$formRegister->children['passwordConf'] = new PasswordField(array('name' => 'passwordConf', 'label' => __('PasswordConfirmation'), 'required' => true));
if( $formRegister->validate($_REQUEST) ){
	if( $_REQUEST['password'] != $_REQUEST['passwordConf']){
		$formRegister->errors[] = __('PasswordsMismatch');
	}else {
		plugin_require('vendor/is_email');
		if( is_email($_REQUEST['email'], true) ){
			if( sql_insert('person', array(
				'given_name' => $_REQUEST['given_name'],
				'family_name' => $_REQUEST['family_name'],
				'gender' => $_REQUEST['gender'],
				'name' => $_REQUEST['given_name'].' '.$_REQUEST['family_name'])) ){

				$person_id = sql_last_id();
				sql_insert('account', $user = array(
					'email' => $_REQUEST['email'],
					'password' => sha1(md5($_REQUEST['password'])),
					'person_id' => $person_id
				));

				session_var_set('auth/user', $user);
				redirect('/cart');
			}
		}else{
			$formRegister->errors[] = __('InvalidEmail');
		}
	}
}

$formAddress = new Form();
$formAddress->name = 'address';
$formAddress->btnLabel = __('Save');
$formAddress->children['street_address'] = new TextField(array('name' => 'street_address', 'label' => __('StreetAddress'), 'required' => true));
$formAddress->children['postal_code'] = new TextField(array('name' => 'postal_code', 'label' => __('PostalCode'), 'maxlength' => 10, 'required' => true));
$formAddress->children['address_locality'] = new TextField(array('name' => 'address_locality', 'label' => __('AddressLocality')));
$formAddress->children['address_region'] = new TextField(array('name' => 'address_region', 'label' => __('AddressRegion')));
$formAddress->children['address_country'] = new SelectField(array('name' => 'address_country', 'label' => __('AddressCountry'), 'datas' => get_country_list()));
if( $formAddress->validate($_REQUEST) ){
	if( sql_insert('contact_point', array(
		'street_address' => $_REQUEST['street_address'],
		'postal_code' => $_REQUEST['postal_code'],
		'address_locality' => $_REQUEST['address_locality'],
		'address_region' => $_REQUEST['address_region'],
		'address_country' => $_REQUEST['address_country']
	)) ){
		$contact_id = sql_last_id();
		sql_insert('person_contact', array('contact_id' => $contact_id, 'person_id' => session_var_get('auth/user/person_id')));
		redirect('/cart');
	}
}

include_once('header.php');

$formSelectAddress = new Form();
$formSelectAddress->name = 'select-address';
$formSelectAddress->btnLabel = __('Update');
if( $formSelectAddress->validate($_REQUEST)){
	
	if( isset($_REQUEST['address'])){
		$cart['delivery_address'] = $_REQUEST['address'];
		session_var_set('cart', $cart);
		redirect('/cart');
	}else{
		print p(__('InvalidAddress'));
	}
}

$formPayment = new Form();
$formPayment->name = 'payment';
//$formPayment->btnLabel = __('Order');
foreach( var_get('config/eshop/paymentMethods') as $pm ){
	$radio = new RadioField(array('name' => 'paymentMethod', 'label' => $pm, 'value' => $pm));
	if( isset($cart['paymentMethod']) && $cart['paymentMethod'] == $pm )
		$radio->attributes['checked'] = true;
	$formPayment->children[] = $radio;
}
if( $formPayment->validate($_REQUEST) ){
	if( isset($_REQUEST['paymentMethod']) ){
		$cart['paymentMethod'] = $_REQUEST['paymentMethod'];
		session_var_set('cart', $cart);
		redirect('/order');
	}
}

print title(__('Cart'), 1);

// - no products
if( !isset($cart['products']) || sizeof($cart['products']) == 0 ){
		print p(__('NoProductInCart'));
}else{
// - list products
	foreach( $cart['products'] as &$prd ){
		if( !isset($prd['price']) )
			$prd['price'] = 0;
		$prd['name'] = '<a href="/prd/'.$prd['id'].'-'.slug($prd['name']).'.html">'.$prd['name'].'</a>';
		$prd['price_str'] = $prd['price'].get_currency_symbol();
		$prd['total'] = ($prd['price'] * $prd['qty']).get_currency_symbol();
		$prd['actions'] = '<a class="btn btn-delete" href="/cart?delete='.$prd['id'].'">'.__('Delete').'</a>';
	}
	$table = array(
		'head' => array(
			'name' => __('ProductName'),
			'price_str' => __('Price'),
			'qty' => __('Quantity'),
			'total' => __('TotalPrice'),
			'actions' => __('Actions')
		),
		'body' => $cart['products'],
		'caption' => __('ProductsInCart')
	);

	
	if( isset($cart['delivery']) ){
		$table['body'][] = array(
			'name' => __('ShippingFees') . ' (' . $cart['delivery']['name'].')',
			'total' => $cart['delivery']['shipping_fees'].get_currency_symbol(),
			//'actions' => '<a href="/cart?setDeliveryMethod=1">'.__('Edit').'</a>'
		);
	}

	if( isset($cart['discount']) ){
		$table['body'][] = array(
			'name' => __('DiscountCode') . ' : ' . $cart['discount']['code'],
			'total' => $cart['discount']['price_value'] > 0 ? '-'.$cart['discount']['price_value'].get_currency_symbol() : '-'.$cart['discount']['price_percent'].'%',
			'actions' => '<a href="/cart?deleteDiscount=1">'.__('Delete').'</a>');
	}
	else
		$table['body'][] = '<td colspan="'.sizeof($table['head']).'">'.$formDiscount->render().'</td>';

	$table['body'][] = array('name' => __('Total'), 'total' => $cart['price'].'€');
	
	print table($table, array('class' => 'table-cart'));

	if ( !session_var_get('auth/user') ){
		print title(__('Account'), 2);

		print $formLogin->render();
		print $formRegister->render();
	}else{
		print title(__('DeliveryAddress'), 2);

		$person_contact = sql_get('person_contact', array('where' => 'person_id='.session_var_get('auth/user/person_id')));
		$contact_ids = array();
		if( $person_contact ){
			foreach ($person_contact as $pc ){
				$contact_ids[] = $pc['contact_id'];
			}
			$addresses = sql_get('contact_point', array('where' => 'id IN ('.implode(',', $contact_ids). ')'));
			if( $addresses ){
				print title(__('MyAddresses'), 3);
				foreach( $addresses as $addr ){
					$radio = new RadioField(array('name' => 'address', 'value' => $addr['id'], 'label' => $addr['street_address'].'<br />'.$addr['postal_code']. ' '.$addr['address_locality'].'<br />'.$addr['address_country']));

					if( isset($cart['delivery_address']) && $cart['delivery_address'] == $addr['id'])
						$radio->attributes['checked'] = true;
					$formSelectAddress->children[] = $radio;
				}
				print $formSelectAddress->render();
			}
		}

		print title(__('NewAddress'), 3);
		print $formAddress->render();
	}

	if( isset($deliveries) && sizeof($deliveries) ){
		print title(__('DeliveryOptions'), 2);
		print $formDelivery->render();
	}

	if( isset($cart['delivery']) ){
		print title(__('PaymentMethods'), 2);
		print $formPayment->render();
	}

	
// - cart actions
	//print '<a class="btn btn-rad" href="/order">'.__('Order now').'</a>';
}

include_once('footer.php');
?>
