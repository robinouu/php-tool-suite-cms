<?php
$parent = isset($_REQUEST['cid']) ? $_REQUEST['cid'] : null;
if( !$parent ){
	$category = null;//sql_get('product_category', array('limit' => 1, 'where' => 'parent_category IS NULL'));
}else{
	$category = sql_get('product_category', array(
		'alias' => 'pc',
		'select' => 'pc.*, m.url as media_url',
		'join' => 'LEFT JOIN media m ON m.id = pc.media',
		'limit' => 1,
		'where' => 'pc.id = ' . sql_quote($parent)));
	$webpage['title'] = $category['name'];
}

include_once('header.php');

if( $category ){
?>
		<div class="section" id="category">
			<h1><?php print $category['name']; ?></h1>
			<p><?php print htmlspecialchars($category['description']); ?></p>
			<?php if( $category['media_url']) { ?><img src="<?php print $category['media_url']; ?>" alt="<?php print $category['name']; ?>" /><?php } ?>
		</div>
<?php

	$breadcrumb = array($category);
	$cat = $category;
	if( $cat['parent_category'] ){
		do {
			$pcat = sql_get('product_category', array('where' => 'id='.$cat['parent_category'], 'limit' => 1));
			if( $pcat ){
				$breadcrumb[] = $pcat;
				$cat = $pcat['parent_category'];
			}
		} while( $pcat && $cat );
	}
	$breadcrumb = array_reverse($breadcrumb);
	$breadcrumb_html = array();
	$breadcrumb_json = array();
	$i = 1;
	foreach( $breadcrumb as $b ){
		$link = '/cat/'.$b['id'].'-'.slug($b['name']).'.html';
		if( $b['id'] !== $category['id'] ){
			$breadcrumb_html[] = '<a href="'.$link.'">'.htmlspecialchars($b['name']).'</a>';
		}
		$breadcrumb_json[] = '{
			"@type": "ListItem",
			"position": '.$i.',
			"name": "'.htmlspecialchars($b['name']).'",
			"item": "'.pts\URI::root().$link.'"
		}';
		++$i;
	}
	print implode('&gt;', $breadcrumb_html);
	?>
	<script type="application/ld+json">
	{
		"@context": "https://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement": [<?php print implode(', ', $breadcrumb_json); ?>]
	}
</script>
	<?php
}


$children = sql_get('product_category', array(
	'alias' => 'pc',
	'select' => 'pc.*, m.url as media_url',
	'where' => $category ? 'parent_category = '.$category['id'] : ' parent_category IS NULL',
	'join' => 'LEFT JOIN media m ON m.id = pc.media'
));
if( $children ) {
?>
		<div class="section" id="children_categories">
<?php
	foreach( $children as $child ){
	?>
			<div class="item item-cat">
				<h2><?php print $child['name']; ?></h2>
				<a href="/cat/<?php print $child['id'].'-'.slug($child['name']); ?>.html" class="overall"></a>
				<?php if( $child['media_url']){ ?><img src="<?php print $child['media_url']; ?>" alt="<?php print $child['name']; ?>" /><?php } ?>
			</div>
	<?php
	}
?>
		</div>
<?php } ?>

<?php
if( $category ){
	$products = sql_get('product', array(
		'alias' => 'p',
		'select' => 'p.*, m.url as media_url',
		'join' => 'LEFT JOIN media m ON m.id = p.media',
		'where' => 'category='.$category['id']));
	if( $products ){
?>
		<div class="section" id="products">
<?php
	foreach( $products as $product ){
	?>
			<div class="item item-product">
				<a href="/prd/<?php print $product['id'].'-'.slug($product['name']); ?>.html" class="overall"></a>
				<?php if( $product['media_url'] ){ ?><div class="thumb">
					<img src="<?php print $product['media_url']; ?>" alt="<?php print $product['name']; ?>" />
				</div><?php } ?>
				<div class="title"><?php print $product['name']; ?></div>
			</div>
	<?php
	}
?>
		</div>
<?php } 
}

include_once('footer.php');

?>