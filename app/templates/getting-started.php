<?php

require_once('header.php');

print title(__('GettingStarted'));

print menu([
	__('Install') => '/getting-started#install',
	__('Templates') => '/getting-started#templates',
	__('Plugins') => '/getting-started#plugins'
]);

print '<div id="install">';
print title(__('Install'), 2);

?>
<p>
	Décompressez l'archive sur votre serveur web, et commencez à personnaliser votre CMS en accédant à l'url relative : '/admin/install'.
</p>

<?php
print '<div>';

print '<div id="plugins">';
print title(__('Plugins'), 2);
?>
<p>Les plugins vous permettront de configurer votre application web. Profitez des plugins pré-existants pour un design fin de votre application ()</p>
<?php

$plugins = glob('plugins/*.php');
foreach ($plugins as &$v) {
	$v = basename($v);
	$v = ucfirst(str_replace('.php', '', $v));
}
print menu($plugins);

print '<div>';

print '<div id="templates">';
print title(__('Templates'), 2);
?>
<p>Les templates sont sauvegardés en base de donnée dans la table <b>webpage</b>. <br />Les fichiers de template, eux, sont sauvegardé dans <b>app/templates/*.php</b>.</p>
<p>Ce sont ces vues qui affichent les informations à l'utilisateur, et il est important que vous ayez le plein contrôle dessus (comme utiliser un moteur de template PHP)</p>

<?php
print '<div>';


require_once('footer.php');