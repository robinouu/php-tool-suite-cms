	
		<div class="section header">
			<<?php print ( $webpage['route'] == '/' ) ? 'h1' : 'div'; ?> class="title">
				<div class="logo"></div>
				<span><?php print $website['title']; ?></span>
			</<?php print ( $webpage['route'] == '/' ) ? 'h1' : 'div'; ?>>
		</div>

<?php if (in_array('auth', var_get('config/core/plugins'))){ ?>
		<nav>
			<ul>
				<?php if( session_var_get('auth/user') ){ 
					$profile = sql_get('person', array('where' => 'id='.sql_quote(session_var_get('auth/user/person_id')), 'limit' => 1));
				?>
				<li><a href="/account/profile"><?php print $profile['name']; ?></a></li>
				<?php } else { ?>
				<li><a href="/signin"><?php print __('SignIn'); ?></a></li>
				<?php } ?>
				<?php if( in_array('eshop', var_get('config/core/plugins')) ){ ?><li><a href="/cart"><?php print __('Cart'); ?></a></li><?php } ?>
			</ul>
		</nav>
<?php } ?>

		<nav>
			<ul>
				<li><a href="#home">Home</a></li>
				<li><a href="/getting-started"><?php print __('GettingStarted'); ?></a></li>
				<?php if( in_array('blog', var_get('config/core/plugins')) ){ ?><li><a href="/blog">Blog</a></li><?php } ?>
				
				<?php if( in_array('eshop', var_get('config/core/plugins')) ){ ?><li><a href="/category">Catalogue</a><?php 
				$cats = sql_get('product_category', array('where' => 'parent_category IS NULL'));
				if( $cats ){
					print '
					<ul class="sub">';
					foreach( $cats as $cat ){
						print '
						<li><a href="/cat/'.$cat['id'].'-'.slug($cat['name']).'.html">'.$cat['name'].'</a>';
						$subcats = sql_get('product_category', array('where' => 'parent_category='.$cat['id']));
						if( $subcats ){
							print '<ul>';
							foreach( $subcats as $subcat ){
								print '<li><a href="/cat/'.$subcat['id'].'-'.slug($subcat['name']).'.html">'.$subcat['name'].'</a></li>';
							}
							print '</ul>';
						}
						print '
						</li>';
					}
					print '
					</ul>';
				}
				?>
				
				</li><?php } ?>
				
				<li><a href="#aboutus">About us</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
		</nav>
