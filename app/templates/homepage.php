	<?php include_once('header.php'); ?>
		<div class="section" id="home">
			<h2>Home</h2>
			<div class="slider">
				<div class="slide">
					<img src="app/assets/img1.jpg" alt="Image jpeg" height="250" />
				</div>
				<div class="slide">
					<img src="app/assets/img1.jpg" alt="Image jpeg" height="250" />
				</div>
				<div class="slide">
					<img src="app/assets/img1.jpg" alt="Image jpeg" height="250" />
				</div>
				<div class="slide">
					<img src="app/assets/img1.jpg" alt="Image jpeg" height="250" />
				</div>
				<div class="slide">
					<img src="app/assets/img1.jpg" alt="Image jpeg" height="250" />
				</div>
			</div>
		</div>

		<div class="section" id="aboutus">
			<h2>About us</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas semper orci sed varius ultricies. Morbi sit amet dignissim orci, et lacinia turpis. Phasellus bibendum fermentum purus, ut ultricies felis dictum eu. Donec hendrerit pulvinar ligula, sit amet cursus nulla eleifend eu. Pellentesque justo risus, mattis eget rhoncus sit amet, ultricies ut nisi. Duis interdum nisi in nulla posuere, vitae hendrerit ligula venenatis. Fusce ut aliquet sem. Nam varius sed massa a fermentum. Phasellus erat sem, aliquam in risus in, dignissim ornare massa. Proin lorem lorem, condimentum vitae velit finibus, eleifend tincidunt lacus. Mauris pharetra, tellus vel congue vehicula, ex eros eleifend justo, at commodo lectus purus non neque. Suspendisse feugiat, est et euismod venenatis, elit elit semper mauris, vel auctor libero risus sed neque. Fusce vitae dolor tempus, viverra quam laoreet, commodo lectus. Phasellus sed facilisis elit. Morbi id vulputate urna. Nam elit est, scelerisque sit amet sem eu, convallis accumsan lectus.</p>
			<p>
			Mauris pulvinar sodales tellus faucibus tempor. Proin nec pretium lorem, ac lobortis turpis. Praesent eget nulla in nisl feugiat hendrerit facilisis nec justo. Morbi vehicula sit amet massa tempus mollis. Vestibulum porta consequat consectetur. Nulla viverra, nisl sed tincidunt cursus, dolor neque molestie dolor, eu porttitor augue nunc a quam. Pellentesque posuere, felis sit amet blandit iaculis, mi lacus convallis magna, sed placerat nisl justo id lectus. Nullam tincidunt risus sed tristique vulputate. Morbi in rhoncus velit, a pellentesque nunc. Quisque quis est id lorem fringilla venenatis. Duis turpis leo, ornare nec odio at, rutrum euismod ex. Sed consectetur, purus vel aliquam consectetur, augue nulla vulputate purus, vel tristique elit augue in quam. Vestibulum facilisis at est a facilisis. Ut eget dapibus nibh. Fusce at facilisis tortor.</p>
			<p>
			Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla leo est, bibendum et odio eget, porta dignissim justo. Maecenas tincidunt iaculis purus, sed finibus massa venenatis at. Nullam convallis dapibus enim, sit amet iaculis leo venenatis ac. Sed vitae diam ipsum. Phasellus sed justo commodo, ullamcorper orci ut, facilisis purus. Praesent nisi diam, sagittis ac lorem sed, ultrices ultricies erat. Mauris non ante ex. Fusce erat leo, fringilla id dui eget, mollis dignissim tellus. Donec euismod tortor eu nulla pellentesque, ut aliquet dolor posuere. Fusce placerat, leo nec semper vestibulum, justo arcu mattis ex, et varius nulla massa id neque.
			</p>
		</div>

		<div class="section" id="contact">
			<h2>Contact us</h2>
		<?php
		$formContact = new Form();
		$formContact->children['fullname'] = new TextField(array('name' => 'fullname', 'label' => __('Fullname')));
		$formContact->children['fullname']->before = '<div class="field">';
		$formContact->children['fullname']->after = '</div>';

		$formContact->children['email'] = new EmailField(array('name' => 'email', 'required' => true, 'label' => __('Email')));
		$formContact->children['email']->before = '<div class="field">';
		$formContact->children['email']->after = '</div>';

		$formContact->children['content'] = new TextAreaField(array('name' => 'content', 'label' => __('Message'), 'size' => 8));
		$formContact->children['content']->before = '<div class="field">';
		$formContact->children['content']->after = '</div>';

		print $formContact->render();
		?>
		</div>
<?php include_once('footer.php'); ?>