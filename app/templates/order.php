<?php 

if( isset($_REQUEST['id']) ){
	$order = sql_get('order', array('where' => 'id='.sql_quote($_REQUEST['id']), 'limit' => 1 ));
	if( !$order ){
		die('Order not found');
	}
}else{


	$order = session_var_get('order');

	if( !$order ){
		$cart = session_var_get('cart');

		$orderCode = sql_get('order', array('limit' => 1, 'orderBy' => 'id DESC'));
		if( !$orderCode )
			$orderCode = 1;
		else
			$orderCode = (int)($orderCode['id'])+1;
		$orderCode = str_pad($orderCode, 8, "0", STR_PAD_LEFT);

		sql_insert('order', $order = array(
			'account_id' => session_var_get('auth/user/id'),
			'order_code' => $orderCode,
			'order_date' => date('Y-m-d H:i:s'),
			'order_status' => 0,
			'discount_id' => isset($cart['discount']) ? $cart['discount'] : null,
			'payment_method' => $cart['paymentMethod'],
			'price' => $cart['price'],
			'delivery_method' => $cart['delivery']['id'],
			'delivery_address' => $cart['delivery_address']
		));
		$order['id'] = sql_last_id();
		session_var_set('order', $order);
		session_var_unset('cart');

		foreach( $cart['products'] as $prd ){
			sql_insert('order_product', array('product_id' => $prd['id'], 'order_id' => $order['id'], 'quantity' => $prd['qty']));
		}
	}
	redirect('/order?id='.$order['id']);
}

include('header.php');

print title(__('Order') . ' : ' . $order['order_code']);
// - order paid
if( $order['order_status'] == 0 ){
	print p('Commande en attente de paiement.');
}
else if( $order['order_status'] == 1 ){
	print p('Commande passée.');
}
else if( $order['order_status'] == 2 ){
	print p('Commande en cours de livraison.');
}
else if( $order['order_status'] == 3 ){
	print p('Commande livrée.');
}

if( isset($order['payment_method']) && $order['order_status'] == 0 ){
	switch ($order['payment_method']){
		case "CreditCard":
//				print $formCreditCard->render();
		break;
		case "Check":
			print p(__('CheckDetails'));
		break;
		case "Deposit":
			print p(__('DepositDetails'));
		break;
	}
}

$order_products_ids = array();
$order_products = sql_get('order_product', array(
	'alias' => 'op',
	'select' => 'p.*, op.quantity',
	'where' => 'order_id='.sql_quote($order['id']),
	'join' => 'INNER JOIN product p ON p.id = op.product_id'
));
if( $order_products ){
	foreach( $order_products as &$prd ){
		$prd['price_str'] = $prd['price'].get_currency_symbol();
		$prd['total'] = ($prd['price']*$prd['quantity']).get_currency_symbol();
	}
}else{
	die(__('NoProductInOrder'));
}

$delivery = sql_get('delivery_method', array('where' => 'id='.sql_quote($order['delivery_method']), 'limit' => 1));

$table = array(
	'head' => array(
		'name' => __('ProductName'),
		'price_str' => __('Price'),
		'quantity' => __('Quantity'),
		'total' => __('TotalPrice'),
		'actions' => __('Actions')
	),
	'body' => $order_products,
	'caption' => __('ProductsInCart')
);


$table['body'][] = array(
	'name' => __('ShippingFees') . ' (' . $delivery['name'].')',
	'total' => $delivery['shipping_fees'].get_currency_symbol(),
);
if( $order['discount_id'] ){
	$discount = sql_get('discount', array('where' => 'id='.$order['discount_id'], 'limit' => 1));
	$table['body'][] = array(
		'name' => __('DiscountCode') . ' : ' . $discount['code'],
		'total' => $discount['price_value'] > 0 ? '-'.$discount['price_value'].get_currency_symbol() : '-'.$discount['price_percent'].'%');
}
$table['body'][] = array('name' => __('Total'), 'total' => $order['price'].'€');

print table($table, array('class' => 'table-cart'));

include('footer.php');

?>
