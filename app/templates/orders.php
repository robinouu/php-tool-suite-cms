<?php
require_once('header.php');
require_once('account.php');

$orders = sql_get('order', array(
	'where' => 'account_id = ' . session_var_get('auth/user/id')
));
foreach( $orders as &$o ){
	$o['order_code_str'] = '<a href="/account/orders?oid='.$o['id'].'" target="_blank">'.$o['order_code'].'</a>';
	$o['price_str'] = $o['price'] . get_currency_symbol();
}

if( isset($_REQUEST['oid']) ){
	eshop_order_pdf();
}
?>

<div class="col-8">
	<h1><?php print __('MyOrders'); ?></h1>
	<?php if( $orders ) { print table(array(
		'head' => array('order_code_str' => __('OrderCode'), 'price_str' => __('Price'), 'order_date' => __('OrderDate')),
		'body' => $orders)
	); } ?>
</div>

<?php

require_once('footer.php');
?>