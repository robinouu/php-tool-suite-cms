<?php

$fmt = new IntlDateFormatter(current_locale(), IntlDateFormatter::FULL, IntlDateFormatter::SHORT);

if( !isset($_REQUEST['pid']))
	die('No article found');

$article = sql_get('article', array('where' => 'id='.sql_quote($_REQUEST['pid']), 'limit' => 1));
if( !$article )
	die('Could not find the article');

$content = sql_get('content', array('where' => 'id='.$article['content'], 'limit' => 1));
$du = new DateTime($content['updated_at']);

$author = sql_get('person', array(
	'alias' => 'p',
	'select' => 'p.*, m.url as media_url',
	'join' => 'LEFT JOIN media m ON m.id = p.image',
	'where' => 'p.id='.$article['author'],
	'limit' => 1
));

$media = null;
if( $article['media'] )
	$media = sql_get('media', array('where' => 'id='.$article['media'], 'limit' => 1));

include('header.php');

?><h1><?php print htmlspecialchars($content['title']); ?></h1>
<p>
	<span>Par <?php print $author['name']; ?></span>,
	<time datetime="<?php print $content['updated_at']; ?>"><?php print $fmt->format($du); ?></time>
</p>
<?php print $content['content']; ?>

<?php
$comments = sql_get('comment', array(
	'alias' => 'c',
	'select' => 'c.*, p.name as author_name',
	'join' => 'LEFT JOIN person p ON p.id = c.author',
	'where' => 'article='.$article['id']
));
if( $comments ){
?>
<section id="comments">
	<?php	foreach( $comments as $com ){ ?>
	<div class="comment">
		<span class="author"><?php print htmlspecialchars($com['author_name']); ?> : </span><span class="msg"><?php print nl2br(htmlspecialchars($com['message'])); ?></span>
	</div>
	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Comment",
		"text" : "<?php print nl2br(strip_tags($com['message'])); ?>",
		"about": "<?php print $content['title']; ?>",
		"author": "<?php print $com['author_name']; ?>",
		"dateCreated": "<?php print $com['posted_at']; ?>",
		"datePublished": "<?php print $com['posted_at']; ?>",
		"dateModified": "<?php print $com['posted_at']; ?>"
	}
	</script>
	<?php } ?>
</section>
<?php } ?>

<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Article",
		"author": "<?php print $author['name']; ?>",
		"name": "<?php print $content['title']; ?>",
		"headline": "<?php print $content['title']; ?>",
		<?php if( $media ){ ?>
			"image": "<?php print pts\URI::root().$media['url']; ?>",
		<?php } else if( $author['media_url'] ) { ?>
			"image:" : "<?php print pts\URI::root().$author['media_url']; ?>",
		<?php } ?>
		"url": "<?php print pts\URI::current(); ?>",
		"text": "<?php print nl2br(strip_tags($content['content'])); ?>",
		"keywords": "<?php print $article['keywords']; ?>",
		"dateCreated": "<?php print $content['created_at']; ?>",
		"datePublished": "<?php print $content['created_at']; ?>",
		"dateModified": "<?php print $content['updated_at']; ?>"
	}
</script>

<?php include('footer.php'); ?>