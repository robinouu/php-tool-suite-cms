<?php
plugin_require('sanitize');

if( !isset($_REQUEST['pid']) || !is_numeric($_REQUEST['pid']) ){
	response_code(404);
	die('No product found.');
}

$product = sql_get('product', array(
	'alias' => 'p',
	'select' => 'p.*, m.url as media_url, c.content as description, b.name as brand_name',
	'join' => '
	LEFT JOIN media m ON m.id = p.media
	LEFT JOIN content c ON c.id = p.content
	LEFT JOIN brand b ON b.id = p.brand',
	'limit' => 1,
	'where' => 'p.id = ' . sql_quote($_REQUEST['pid'])));
if( !$product ){
	die('No product found. See "eshop" plugin datas.');
}

$medias = sql_get('media', array('where' => 'parent='.$product['media']));
var_set('webpage/title', $product['name']);
var_set('webpage/description', substr(strip_tags($product['description']), 0, 255));

//$webpage['title'] = $product['name'];

$formBuy = new Form();
$formBuy->name = 'buy';
$formBuy->btnLabel = __('AddToCart');
if( $product['stock'] == '' )
	$_REQUEST['qty'] = 1;
else
	$formBuy->children['qty'] = new NumberField(array('name' => 'qty', 'value' => 1, 'label' => __('Quantity'), 'maxValue' => $product['stock']));
if( $formBuy->validate($_REQUEST)){
	$cart = session_var_get('cart', array());
	$hasOne = false;
	if( isset($cart['products']) ){
		foreach( $cart['products'] as &$cart_product){
			if( $cart_product['id'] == $product['id']){
				$hasOne = true;
				$cart_product['qty'] += (int)$_REQUEST['qty'];
				$cart_product['qty'] = min($cart_product['qty'], $product['stock']);
			}
		}
	}
	if( !$hasOne ){
		if( !isset($cart['products']) )
			$cart['products'] = array();
		$cart['products'][] = array_merge($product, array('qty' => (int)$_REQUEST['qty']));
	}
	session_var_set('cart', $cart);
	var_set('productAddedToCart', true);
}

include('header.php');

$breadcrumb = array();
$cat = $product['category'];
do {
	$pcat = sql_get('product_category', array('where' => 'id='.$cat, 'limit' => 1));
	if( $pcat ){
		$breadcrumb[] = $pcat;
		$cat = $pcat['parent_category'];
	}
} while( $pcat && $cat );

$breadcrumb = array_reverse($breadcrumb);
$breadcrumb_html = array();
$breadcrumb_json = array();
$i = 1;
foreach( $breadcrumb as $b ){
	$link = '/cat/'.$b['id'].'-'.slug($b['name']).'.html';
	$breadcrumb_html[] = '<a href="'.$link.'">'.htmlspecialchars($b['name']).'</a>';
	$breadcrumb_json[] = '{
		"@type": "ListItem",
		"position": '.$i.',
		"name": "'.htmlspecialchars($b['name']).'",
		"item": "'.pts\URI::root().$link.'"
	}';
	++$i;
}
print implode('&gt;', $breadcrumb_html);

?>
<script type="application/ld+json">
	{
		"@context": "https://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement": [<?php print implode(', ', $breadcrumb_json); ?>]
	}
</script>
<div class="section" id="product">
<?php
if( var_get('productAddedToCart') ){ ?>
	<div class="msg-success">Produit(s) ajouté à votre panier.</div>
<?php } ?>
	<h1><?php print $product['name']; ?></h1>
<?php if( $medias ){ ?>
	<div class="gallery">
		<ul>
			<?php foreach( $medias as $media ){ ?>
				<li><a href="<?php print $media['url']; ?>" title="<?php print __('ZoomIn'); ?>"><img src="<?php print $media['url']; ?>" alt="<?php print htmlspecialchars($media['name']); ?>" /></a>
			<?php } ?>
		</ul>
	</div>
<?php }else if( $product['media_url'] ){ ?>
	<div class="gallery">
		<ul>
			<li><a href="<?php print $product['media_url']; ?>" title="<?php print __('ZoomIn'); ?>"><img src="<?php print $product['media_url']; ?>" alt="<?php print __('ProductImage'); ?>" /></a>
			</li>
		</ul>
	</div>
<?php } ?>
	<div class="product-sell-info">
		<i class="price"><?php print $product['price']; ?><span>€</span></i>
	</div>
	<div class="description">
		<?php print htmlspecialchars($product['description']); ?>
	</div>
	<div class="actions">
<?php
if( $product['stock'] == '' || $product['stock'] > 0 || $product['virtual'] ){
	if( $product['stock'] != '' )
		print p(__('AvailableProduct'));
	print $formBuy->render();
	$inStock = true;
} else{
	print p(__('UnavailableProduct'));
	$inStock = false;
}
?>
	</div>
	<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Product",
		"name" : "<?php print $product['name']; ?>",
		"url" : "<?php print pts\URI::current(); ?>",
		"brand" : "<?php print $product['brand_name']; ?>",
		"sku" : "<?php print $product['code']; ?>",
		"description" : "<?php print strip_tags($product['description']); ?>",
		"image" : "<?php print $medias ? current($medias) : pts\URI::root().$product['media_url']; ?>",
		"offers": {
			"@type": "Offer",
			"availability" : "<?php print $inStock ? 'http://schema.org/InStock' : "http://schema.org/OutOfStock"; ?>",
			"price": "<?php print number_format($product['price'], 2, '.', ''); ?>",
			"priceCurrency": "<?php print var_get('config/eshop/defaultCurrency', 'EUR'); ?>"
		}
	}
	</script>
</div>

<?php
$products = sql_get('product', array('where' => 'category='.$product['category']. ' AND id <> ' . $product['id']));
if( $products ){ 
?>
<div class="section" id="similar_products">
	<h2><?php print __('SimilarProducts'); ?></h2>
<?php foreach( $products as $product ){	?>
	<div class="item item-product">
		<a href="<?php print $product['id']; ?>-<?php print slug($product['name']); ?>.html" class="overall"></a>
		<div class="thumb">
			<img src="<?php print $product['image']; ?>" alt="<?php print $product['name']; ?>" />
		</div>
		<div class="title"><?php print $product['name']; ?></div>
	</div>
	<?php
}
?>
</div>
<?php } 

include('footer.php');

?>