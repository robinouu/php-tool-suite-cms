<?php
require_once('header.php');
require_once('account.php');

$yearsDatas = array();
for( $i = date('Y'); $i > date('Y')-120; --$i){
	$yearsDatas[$i.''] = $i.'';
}
$monthsDatas = array();
for( $i = 1; $i < 13; ++$i ){
	$monthsDatas[$i.''] = $i.'';
}
$daysDatas = array();
for( $i = 1; $i < 32; ++$i){
	$daysDatas[$i.''] = $i.'';
}

$profile = sql_get('person', array('where' => 'id='.session_var_get('auth/user/person_id'), 'limit' => 1));

$formProfile = new Form();
$formProfile->name = 'profile';
$formProfile->btnLabel = __('Save');

$birthday = new DateTime($profile['birth_date']);

$formProfile->children['birthdate_day'] = new SelectField(array('name' => 'birthdate_day',  'label' => __('Birthday'), 'datas' => $daysDatas, 'value' => $birthday->format('d')));
$formProfile->children['birthdate_day']->before = '<div class="field">';
$formProfile->children['birthdate_month'] = new SelectField(array('name' => 'birthdate_month', 'label_position' => 'none', 'datas' => $monthsDatas, 'value' => $birthday->format('m')));
$formProfile->children['birthdate_year'] = new SelectField(array('name' => 'birthdate_year', 'label_position' => 'none', 'datas' => $yearsDatas, 'value' => $birthday->format('Y')));
$formProfile->children['birthdate_year']->after = '</div>';

$formProfile->children['gender'] = new SelectField(array('name' => 'gender', 'label' => __('Gender'), 'datas' => array('male' => __('MaleCivility'), 'female' => __('FemaleCivility')), 'value' => $profile['gender']));

$formProfile->children['given_name'] = new TextField(array('name' => 'given_name', 'label' => __('GivenName'), 'value' => $profile['given_name']));
$formProfile->children['family_name'] = new TextField(array('name' => 'family_name', 'label' => __('FamilyName'), 'value' => $profile['family_name']));
$formProfile->children['description'] = new TextAreaField(array('name' => 'description', 'label' => __('Description'), 'value' => $profile['description']));

if( $formProfile->validate($_REQUEST) ){
	$bd = new DateTime($_REQUEST['birthdate_year'].'-'.$_REQUEST['birthdate_month'].'-'.$_REQUEST['birthdate_day']);
	sql_update('person', array(
		'birth_date' => $bd->format('Y-m-d H:i:s'),
		'gender' => $_REQUEST['gender'],
		'given_name' => $_REQUEST['given_name'],
		'family_name' => $_REQUEST['family_name'],
		'description' => $_REQUEST['description']
	), 'id='.session_var_get('auth/user/person_id'));
	redirect('/account/profile');
}
?>
<div class="col-8">
	<h1><?php print __('MyProfile'); ?></h1>
	<?php print $formProfile->render(); ?>
</div>

<?php

require_once('footer.php');
?>