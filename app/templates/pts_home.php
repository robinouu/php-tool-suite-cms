<h1>PHP Tool Suite</h1>

<?php
plugin_require('doc');
$doc = doc_from_dir(dirname(__FILE__).'/../../../ici-sorties/lib/');

$html = '';
if( isset($_REQUEST['subpackage']) ){
	$html = title($_REQUEST['subpackage'], 2);
	foreach( $doc as $d){
		if (!isset($d['function']) )
			continue;
		if( isset($d['subpackage'][0]) && $d['subpackage'][0] == $_REQUEST['subpackage'] ){
			$html .= '<h3>'.$d['function'].'</h3>';
			$html .= '<p>'.$d['summary'].'</p>';
			if( isset($d['param']) ){
				$html .= '<p><strong>Parameters</strong><ul>';
				foreach( $d['param'] as $p ){
					$html .= '<li>'.$p.'</li>';
				}
				$html .= '</ul></p>';
			}
			if( isset($d['return']) ){
				$html .= '<p><strong>Returns</strong> : ';
				foreach( $d['return'] as $p ){
					$html .= $p;
				}
				$html .= '</p>';
			}
		}
	}
	print $html;
}else{
?>

<h2>as library</h2>
<p>PHP Tool Suite includes capabilities such as </p>
<?php
	$packages = array();
	$html = '<ul>';
	foreach( $doc as $d ){
		if( isset($d['subpackage']) && !in_array($d['subpackage'][0], $packages))
			$packages[] = $d['subpackage'][0];
		//var_dump($d['subpackage']);
	}
	foreach( $packages as $package ){
		$html .= '<li><a href="?subpackage='.$package.'">'.$package.'</a></li>';
	}
	$html .= '</ul>';
	print $html;
	?>
	<h2>as content management system</h2>
	<p>It includes
	<ul>
		<li>Plugin management (install, load and uninstall)</li>
		<li>HTTP Authentication</li>
		<li>Data management</li>
	</ul>
	<?php
}


?>