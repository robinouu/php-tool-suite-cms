<?php

require_once('header.php');

$formSignin = new Form();
$formSignin->children['email'] = new EmailField(array('name' => 'email', 'label' => __('Email')));
$formSignin->children['password'] = new PasswordField(array('name' => 'password'));
if( $formSignin->validate($_REQUEST) ){
	$user = auth_identify($_REQUEST['email'], $_REQUEST['password']);
	if( $user ){
		session_var_set('auth/user', $user);
		redirect('/account/profile');
	}
}
?>
<div class="col-12">
	<h1><?php print __('SignIn'); ?></h1>
	<?php print $formSignin->render(); ?>
</div>

<?php
require_once('footer.php');
?>