<?php

// Get the website
$url = pts\URI::root();
$website = sql_get('website', array('where' => 'url='.sql_quote($url), 'limit' => 1));
if( !$website ){
	die('No website found in database for url "<i>'.pts\URI::root().'</i>". See "datas" plugin.');
}

$website_media = $website['media'] ? sql_get('media', array('where' => 'id='.$website['media'])) : null;

// Get the webpage
$route = request_route();

$webpage = sql_get('webpage', array('where' => 'id_website='.(int)$website['id'].' AND route = '.sql_quote($route), 'limit' => 1));
if( !$webpage ){
	die('No webpage found for the route "<i>'.$route.'</i>". See "datas" plugin.');
}

// Set the HTML page options

$stylesFunc = function (){
		
	$style = new stdclass();
	$style->href = 'app/css/all.css?v='.uniqid();
	$style->type = 'text/css';
	$style->rel = 'stylesheet';

	return array($style);
};
on('webpage/styles', $stylesFunc);

ob_start();
require_once('app/templates/'.$webpage['template'].'.php');
$body = ob_get_contents();
ob_end_clean();

$metas = array(
	'author' => $website['author'],
	'generator' => trim($website['generator']) != '' ? $website['generator'] : 'Php Tool Suite',
	'description' => var_get('webpage/description', $website['description']),
	'keywords' => $website['keywords'],
	'referrer' => 'origin',
	'color-scheme' => 'normal',
	"twitter:card" => "summary",
	"twitter:site" => $website['title'],
	"twitter:title" => var_get('webpage/title', $website['title']),
	"twitter:description" => var_get('webpage/description', $website['description']),
	"twitter:creator" => $website['author'],
	'
		<meta property="og:title" content="'.var_get('webpage/title', $website['title']).'">
		<meta property="og:site_name" content="'.$website['title'].'">
		<meta property="og:url" content="'.pts\URI::root().'">
		<meta property="og:description" content="'.var_get('webpage/description', $website['description']).'">
		'.($website_media ? '<meta property="og:image" content="'.$website_media['url'].'">' : '').'
		<meta property="og:type" content="website">'
);
if( $website_media )
	$medias["twitter:image"] = $website_media['url'];

$styles = trigger('webpage/styles');

$page = new pts\Webpage(array(
	'lang' => 'en',
	'dir' => 'ltr',
	'title' => var_get('webpage/title', $website['title']),
	'base.target' => '_self',
	'nomodule' => null,
	'meta' => $metas,
	'css' => $styles,
	'javascript' => array(),
));
$page->body = $body;

print $page;

?>