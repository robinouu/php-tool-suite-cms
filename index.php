<?php
// --- init
session_start();
require_once(dirname(__FILE__).'/../ici-sorties/lib/core.inc.php');
require_once('lib/Webpage.php');
require_once('lib/Content.php');
require_once('lib/URI.php');
plugin_require(array('sql', 'html', 'i18n', 'file', 'response'));

// --- config
var_set('sql/dump', false);

$configPath = dirname(__FILE__).'/config.php';
if( !is_file($configPath) ){
	require_once('plugins/core.php');
	trigger('plugin_install');
	redirect('/admin/install');
}
else
	$config = require_once($configPath);

var_set('config', $config);

// --- plugins
ob_start();
foreach( $config['core']['plugins'] as $plugin ){
	//off('plugin_load');
	require_once('plugins/'.$plugin.'.php');
}
trigger('plugin_load');
$content = ob_get_contents();
ob_end_clean();

// --- main

route('/admin.*', function () use(&$content){

	$style = new stdclass();
	$style->href = 'app/css/admin.css?v='.uniqid();
	$style->type = 'text/css';
	$style->rel = 'stylesheet';

	$webpage = new pts\Webpage(array('css' => array($style), 'javascript' => trigger('webpage/javascript', array())));
	$webpage->title = var_get('webpage/title') . ' - Admin';
	$webpage->body = $content;
	print $webpage;
});

if( in_array('datas', $config['core']['plugins']) ){
	response_no_route(function (){
		require_once('app/templates/webpage.php');
	});
}


?>