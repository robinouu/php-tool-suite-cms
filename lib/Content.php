<?php

namespace pts {
	class Content{
		private $buffer;
		public function __construct($filePath){
			ob_start();
			@include($filePath);
			$this->buffer = ob_get_contents();
			ob_end_clean();
		}
		public function __toString(){
			return $this->buffer;
		}
	}
}