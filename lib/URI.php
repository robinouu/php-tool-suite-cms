<?php 

namespace pts {
	class URI {
		public $components;
		public function __construct($str=''){
			$this->components = parse_url($str);
		}
		public function __set($name, $value){
			$this->components[$name] = $value;
		}
		public function __unset($name){
			unset($this->components[$name]);
		}
		public function __isset($name){
			return isset($this->components[$name]);
		}
		public function __get($name){
			return $this->components[$name];
		}
		public function __toString(){
			$res = '';
			if( isset($this->components['scheme']) )
				$res .= $this->components['scheme'].'://';
			if( isset($this->components['host']) )
				$res .= $this->components['host'];
			if( isset($this->components['path']) )
				$res .= $this->components['path'];
			return $res;
		}
		static public function compare($uri1, $uri2, $soft=true){
			if( isset($uri1->scheme) && isset($uri2->scheme) && $uri1->scheme !== $uri2->scheme)
				return false;
			if( isset($uri1->host) && isset($uri2->host) && $uri1->host !== $uri2->host )
				return false;
			if( $uri1->path !== $uri2->path )
				return false;
			if( !$soft ){
				if( (isset($uri1->query) && !isset($uri2->query)) || (!isset($uri1->query) && isset($uri2->query) ))
					return false;
				if( isset($uri1->query) && isset($uri2->query) && $uri1->query !== $uri2->query)
					return false;
			}
			return true;
		}
		static function current(){
			$serverName = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
			$serverPort = isset($_SERVER['SERVER_PORT']) ? $_SERVER['SERVER_PORT'] : '';
			$serverURI = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
			$strUrl = is_server_secure() ? 'https://' : 'http://';
			if ($serverPort != '80') {
				$strUrl .= $serverName.':'.$serverPort.$serverURI;
			} else {
				$strUrl .= $serverName.$serverURI;
			}
			return new URI($strUrl);
		}
		static function root(){
			$url = self::current();
			unset($url->path);
			return $url;
		}
	}
}