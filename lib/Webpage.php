<?php

namespace pts {

	class Webpage {
		private $options = array();
		private $packages = array();
		public function __construct($options=array()){
			$this->options = array_replace_recursive(
				array(
					'lang' => current_lang(),
					'dir' => 'ltr',
					'title' => '',
					'base.target' => '_self',
					'nomodule' => null,
					'meta' => array(
						'author' => 'Unknow',
						'generator' => 'Unknow',
						'description' => '',
						'keywords' => 'website,webpage',
						'referrer' => 'origin',
						'color-scheme' => 'normal',
					),
					'css' => array(),
					'javascript' => array(),
					'body' => ''
				),
				$options
			);
		}
		public function __set($name, $value){
			$this->options[$name] = $value;
		}
		public function __get($name){
			return $this->options[$name];
		}
		private function _getMetaParts(){
			$html = '';
			foreach( $this->options['meta'] as $name => $content){
				if( is_numeric($name) )
					$html .= $content;
				else
					$html .= '
		<meta name="'.$name.'" content="'.$content.'" />';
			}
			return $html;
		}
		private function _getCSSParts(){
			$html = '';
			foreach( $this->options['css'] as $entry){
				if( isset($entry->href) )
					$html .= '
		<link'.
					(isset($entry->as) ? ' as="'.$entry->as.'"': '').
					(isset($entry->crossorigin) ? ' crossorigin="'.$entry->crossorigin.'"': '').
					(isset($entry->disabled) ? ' disabled="'.$entry->disabled.'"': '').
					(isset($entry->href) ? ' href="'.$entry->href.'"': '').
					(isset($entry->hreflang) ? ' hreflang="'.$entry->hreflang.'"': '').
					(isset($entry->importance) ? ' importance="'.$entry->importance.'"': '').
					(isset($entry->integrity) ? ' integrity="'.$entry->integrity.'"': '').
					(isset($entry->media) ? ' media="'.$entry->media.'"': '').
					(isset($entry->referrerpolicy) ? ' referrerpolicy="'.$entry->referrerpolicy.'"': '').
					(isset($entry->rel) ? ' rel="'.$entry->rel.'"': '').
					(isset($entry->sizes) ? ' sizes="'.$entry->sizes.'"': '').
					(isset($entry->title) ? ' title="'.$entry->title.'"': '').
					(isset($entry->types) ? ' type="'.$entry->type.'"': '');
					$html .= ' />';
			}
			return $html;
		}
		private function _getScriptParts(){
			$html = '';
			foreach( $this->options['javascript'] as $entry ){
				if( isset($entry->module) )
					$html.= '<script type="module" src="'.$entry->module.'"></script>';
				else if( isset($entry->src) )
					$html .= '<script'.
					(isset($entry->async) ? ' async="'.$entry->async.'"' : '').
					(isset($entry->crossorigin) ? ' crossorigin' : '').
					(isset($entry->defer) ? ' defer' : '').
					(isset($entry->integrity) ? ' integrity="'.$entry->integrity.'" ' : '').
					(isset($entry->nonce) ? ' nonce="'.$entry->nonce.'"' : '').
					(isset($entry->referrerpolicy) ? ' async="'.$entry->referrerpolicy.'"' : '').
					(isset($entry->src) ? ' src="'.$entry->src.'"' : '').
					(isset($entry->type) ? ' type="'.$entry->type.'"' : '').
					'></script>
		';
				else if ( isset($entry->content) && is_string($entry->content) ){
					$html .= '<script type="text/javascript">'.$entry->content.'</script>
		';
				}
			}
			if( is_string($this->options['nomodule']) )
				$html .= '<script nomodule src="'.$this->options['nomodule'].'">';
			return $html;
		}
		public function __toString(){
			require_once('URI.php');
			return '<!DOCTYPE html>
<html lang="'.$this->options['lang'].'" dir="'.$this->options['dir'].'">
	<head>
		<meta charset="UTF-8" />
		'.$this->_getMetaParts().'

		<base href="'.URI::root().'" target="'.$this->options['base.target'].'">
		<title>'.$this->options['title'].'</title>
		
		'.$this->_getCSSParts().'
		'.$this->_getScriptParts().'
	</head>
	<body>
	'.$this->options['body'].'
	</body>
</html>';
		}
		/**
		 * Returns an HTML tag
		 * @param string $tag The tag name.
		 * @param string $content The tag content.
		 * @param array $attrs The tag attributes
		 * @param boolean $inline If TRUE if specified, the HTML inline format will be used. (for tags like link,br,hr...)
		 * @return a properly formatted HTML tag
		 * @subpackage HTML
		 */
		static function tag($tag, $content, $attrs = array(), $inline = false) {
			if( $inline ){
				return '<' . $tag . (sizeof($attrs) ? ' ' . attrs($attrs) : '') . ' />';
			}else{
				return '<' . $tag . (sizeof($attrs) ? ' ' . attrs($attrs) : '') . '>' . $content . '</' . $tag . '>';
			}
		}
	}
}
