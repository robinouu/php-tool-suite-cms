<?php

load_translations(array('translations' => array(
	'Connect' => array('fr' => 'Se connecter'),
	'Password' => array('fr' => 'Mot de passe'),
	'Account' => array('fr' => 'Compte utilisateur'),
	'PasswordConfirmation' => array(
		'fr' => 'Confirmation du mot de passe',
		'en' => 'Password confirmation'
	),
	'CreateAccount' => array(
		'fr' => 'Créer le compte',
		'en' => 'Create account'
	),
	'PasswordsMismatch' => array(
		'fr' => 'Les mots de passe ne correspondent pas.',
		'en' => 'Passwords do not match.'
	),
	'InvalidEmail' => array(
		'fr' => 'L\'email est incorrect.',
		'en' => 'Invalid email.'
	),
	'MyProfile' => array(
		'fr' => 'Mon profil',
		'en' => 'My profile'
	),
	'MyOptions' => array(
		'fr' => 'Mes options',
		'en' => 'My options'
	),
	'Disconnection' => array(
		'fr' => 'Déconnexion'
	),
	'AuthenticationPluginDesc' => array(
		'fr' => 'Plugin d\'authentification au site internet.',
		'en' => 'Authentication plugin for the website.'
	)
)));
$plugin = function (){
	return array(
		'id' => 'auth',
		'name' => 'Authentication',
		'description' => 'AuthenticationPluginDesc',
		'requirements' => array('datas'),
		'options' => array(
			'areas' => array(
				'basicRealm' => array('method' => 'basic', 'accounts' => array('admin' => 'admin'), 'authMessage' => 'Connexion non autorisée', 'routes' => array('/admin/?.*'))/*,
				'digestRealm' => array('method' => 'digest', 'accounts' => array('admin' => 'admin'), 'authMessage' => 'Connexion non autorisée', 'routes' => array('/admin/?.*'))*/
			)
		)
	);
};
on('plugin', $plugin);


if (!function_exists('auth_identify')){
	function auth_identify($email, $password){
		$user = sql_get('account', array(
			'where' => 'email='.sql_quote($email).' AND provider = "internal" AND password='.sql_quote(sha1(md5($password))),
			'limit' => 1
		));
		return $user;
	}
}

if (!function_exists('auth_check_role') ){
	function auth_check_role($roles=array(), $userId=null){
		$roles = array();
		if( is_null($userId) )
			$userId = session_var_get('auth/user/id');
		if( is_string($roles) )
			$roles = array($roles);
		$account_roles = sql_get('account_role', array(
			'alias' => 'ar',
			'select' => 'ar.*',
			'join' => 'INNER JOIN role r ON ar.role = r.id',
			'where' => 'ar.account='.sql_quote($userId).' AND r.title IN (\`'.implode("\`,\`", $roles).'\`)'
		));
		return is_array($account_roles) && sizeof($account_roles) > 0;
	}
}

$plugin_install = function (){
	
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		$schema = array_merge($schema, array(
			array(
				'name' => 'account',
				'columns' => array(
					'email VARCHAR(255)',
					'provider VARCHAR(255) DEFAULT "internal"',
					'password VARCHAR(255)',
					'person_id INT(11)'
				),
				'uniqueKeys' => array('email'),
				'foreignKeys' => array('person_id' => 'person(id) ON DELETE CASCADE'),
				'stringKey' => 'email'
			),
			array(
				'name' => 'role',
				'columns' => array(
					'title VARCHAR(255)'
				),
				'stringKey' => 'title'
			),
			array(
				'name' => 'role_right',
				'columns' => array(
					'role INT(11)',
					'title VARCHAR(255)'
				),
				'stringKey' => 'title',
				'foreignKeys' => array('role' => 'role(id) ON DELETE CASCADE')
			),
			array(
				'name' => 'account_role',
				'columns' => array(
					'account INT(11)',
					'role INT(11)'
				),
				'foreignKeys' => array(
					'account' => 'account(id) ON DELETE CASCADE',
					'role' => 'role(id) ON DELETE CASCADE'
				)
			)
		));
		json_save($path, $schema, true);
	}
};
on('plugin_install', $plugin_install);

$plugin_uninstall = function (){
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		foreach( $schema as $k => $v){
			if( in_array($v['name'], array('account', 'role', 'role_right', 'account_role')) )
				unset($schema[$k]);
		}
		json_save($path, $schema, true);
	}
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_load = function (){
	$auth = var_get('config/auth');
	if( !$auth )
		return false;
	
	$passwordFunc = function(){
		return new PasswordField(array('name' => 'password', 'label_position' => 'none'));
	};
	on('datas/account/password', $passwordFunc);

	foreach ($auth['areas'] as $realm => $area) {
		if( !isset($area['routes']) || !is_array($area['routes']) )
			continue;
		$routingFunc = function() use ($area, $realm) {
			if( !isset($area['method']) || $area['method'] === 'basic' ){
				$username = $password = null;
				if( isset($_SERVER['PHP_AUTH_USER']) ){
					$username = $_SERVER['PHP_AUTH_USER'];
					if( isset($_SERVER['PHP_AUTH_USER']) ){
						$password = $_SERVER['PHP_AUTH_PW'];
					}
				}elseif( isset($_SERVER['HTTP_AUTHORIZATION']) && strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']), 'basic') === 0){
					list($username, $password) = explode(':', base64_decode(trim(substr($_SERVER['HTTP_AUTHORIZATION'], 5))));
				}
				
				if( is_null($username) || !array_key_exists($username, $area['accounts']) || $password !== $area['accounts'][$username] ){
					header('HTTP/1.0 401 Unauthorized');
					header('WWW-Authenticate: Basic realm="' . $realm);
					
					print isset($area['authMessage']) ? $area['authMessage'] : 'HTTP/1.0 401 Unauthorized';
					die;
				}

			}elseif( $area['method'] === 'digest' ){

				$requireLogin = function ($realm, $nonce, $msg = null){
					header('WWW-Authenticate: Digest realm="'.$realm.'",qop="auth",nonce="'.$nonce.'",opaque="'.md5($realm).'"');
					header('HTTP/1.1 401 Unauthorized');
					
					print is_string($msg) ? $msg : 'HTTP/1.0 401 Unauthorized';
					die;
				};

				$digest = '';
				if( isset($_SERVER['PHP_AUTH_DIGEST']) ){
					$digest = $_SERVER['PHP_AUTH_DIGEST'];
				}elseif( isset($_SERVER['HTTP_AUTHORIZATION']) && strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']), 'digest') === 0){
					$digest = trim(substr($_SERVER['HTTP_AUTHORIZATION'], 6));
				}

				if( !$digest ){
					$requireLogin($realm, uniqid(), isset($area['authMessage']) ? $area['authMessage'] : null);
				}

				$parts = array('nonce' => 1, 'nc' => 1, 'cnonce' => 1, 'qop' => 1, 'username' => 1, 'uri' => 1, 'response' => 1);
				$keys = implode('|', array_keys($parts));

				preg_match_all('#(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))#', $digest, $matches, PREG_SET_ORDER);

				$data = array();
				foreach ($matches as $m) {
					$data[$m[1]] = $m[3] ? $m[3] : $m[4];
					unset($parts[$m[1]]);
				}
				if( $parts || !array_key_exists($data['username'], $area['accounts']) ){
					$requireLogin($realm, uniqid(), isset($area['authMessage']) ? $area['authMessage'] : null);
				}

				// Generate response
				$A1 = md5($data['username'] . ':' . $realm . ':' . $area['accounts'][$data['username']]);
				$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
				$response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

				if( $data['response'] !== $response ){
					$requireLogin($realm, uniqid(), isset($area['authMessage']) ? $area['authMessage'] : null);
				}
			}elseif( $area['method'] === 'sql'){
				
				$authentication = false;
				if( session_var_get('auth/user') )
					$user = session_var_get('auth/user');
				else if( isset($_REQUEST['email']) && isset($_REQUEST['password'])){
					$user = auth_identify($_REQUEST['email'], $_REQUEST['password']);
				}else
					$user = null;

				if( $user ){
					if( isset($area['roles']) && is_array($area['roles'])){
						$authentication = auth_check_role($area['roles'], $user['id']);
					}else{
						$authentication = true;
					}
				}
				if( !$authentication )
				{
					header('HTTP/1.1 401 Unauthorized');
					die($area['authMessage']);
				}else{
					session_var_set('auth/user', $user);
				}
			}
		};
		foreach( $area['routes'] as $route ){
			route($route, $routingFunc);
		}
	}
};
on('plugin_load', $plugin_load, -100);


if( !function_exists('auth_has_right') ){
	function auth_has_right($right, $userId=null){
		if( is_null($userId) )
			$userId = session_var_get('user/id');
		$get_rights = sql_get('role_right', array(
			'alias' => 'rr',
			'select' => 'rr.title as right',
			'join' => 'INNER JOIN account_role ar ON ar.role = rr.role',
			'having' => 'ar.account = '.$userId
		));
//		var_dump($get_rights);
		foreach( $get_rights as $r ){
			if ($r['right'] == $right)
				return true;
		}
		return false;
	}
}