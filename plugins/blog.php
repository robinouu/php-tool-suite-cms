<?php
load_translations(array('translations' => array(
	'BlogPluginDesc' => array(
		'fr' => 'Plugin pour la création d\'un blog',
		'en' => 'Multi-user news plugin.'
	)
)));
$plugin = function (){
	return array(
		'id' => 'blog',
		'name' => 'Blog',
		'description' => 'BlogPluginDesc',
		'requirements' => array('datas'),
		'options' => array(
		)
	);
};
on('plugin', $plugin);

$plugin_install = function (){
	
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		$schema = array_merge($schema, array(
			array(
				'name' => 'article',
				'columns' => array(
					'author INT(11)',
					'media INT(11)',
					'content INT(11)',
					'keywords TEXT'
				),
				'foreignKeys' => array(
					'author' => 'person(id)',
					'media' => 'media(id)',
					'content' => 'content(id)'
				),
				'stringKey' => 'content'
			),
			array(
				'name' => 'comment',
				'columns' => array(
					'article INT(11)',
					'author INT(11)',
					'message TEXT',
					'posted_at DATETIME'
				),
				'foreignKeys' => array(
					'author' => 'person(id)',
					'article' => 'article(id)'
				),
				'stringKey' => 'name'
			)
		));
		json_save($path, $schema, true);
	}

};
on('plugin_install', $plugin_install);

$plugin_uninstall = function (){
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		foreach( $schema as $k => $v){
			if( in_array($v['name'], array('article', 'comment')) )
				unset($schema[$k]);
		}
		json_save($path, $schema, true);
	}
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_load = function (){
	$blog = var_get('config/blog');
	
	$rules = function (){
		return array('RewriteRule ^post/([0-9]+)\-([^\.]+)\.html$ /post?pid=$1 [L]');
	};
	on('core/htaccess', $rules, 10);

	$frontStyles = function (){
		$style = new stdclass();
		$style->href = 'app/css/blog.css';
		$style->type = 'text/css';
		$style->rel = 'stylesheet';
		return array($style);
	};
	on('webpage/styles', $frontStyles);

	if( !$blog )
		return false;

};
on('plugin_load', $plugin_load);