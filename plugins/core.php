<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

plugin_require(array('widgets', 'response', 'event', 'file'));

load_translations(array('translations' => array(
	'Use' => array('fr' => 'Utiliser'),
	'Insert' => array('fr' => 'Insérer'),
	'Edit' => array('fr' => 'Modifier'),
	'Update' => array('fr' => 'Mettre à jour'),
	'Save' => array('fr' => 'Enregistrer'),
	'Delete' => array('fr' => 'Supprimer'),
	'Install' => array('fr' => 'Installation'),
	'CorePluginDesc' => array(
		'fr' => 'Plugin obligatoire pour le site internet.',
		'en' => 'Core plugin for the website.'
	),
	'Dashboard' => array('fr' => 'Tableau de bord')
)));

$plugin = function (){
	return array(
		'id' => 'core',
		'name' => 'Core',
		'description' => 'CorePluginDesc',
		'options' => array(
			'plugins' => array()
		)
	);
};
on('plugin', $plugin);

$plugin_install = function () use (&$rules) {

	$config = array('core' => array('plugins' => array('core')));
	file_put_contents(dirname(__FILE__).'/../config.php', '<?php return json_decode(\''.json_encode($config, JSON_PRETTY_PRINT).'\', true); ?>');
};
on('plugin_install', $plugin_install);

$plugin_uninstall = function (){
	print 'Désinstallation du plugin "core"';
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_load = function (){
	
	$rules_p1 = function (){
		return array(
			'RewriteEngine on'
		);
	};
	on('core/htaccess', $rules_p1);

	$rules_p2 = function (){
		return array(
			'RewriteRule ^index\.php$ - [L]',
			'RewriteCond %{REQUEST_FILENAME} !-f',
			'RewriteCond %{REQUEST_FILENAME} !-d',
			'RewriteRule . /index.php [L]'
		);
	};
	on('core/htaccess', $rules_p2, 1000);

	$rulesToWrite = trigger('core/htaccess');
	if( $rulesToWrite )
		file_put_contents(dirname(__FILE__).'/../.htaccess', implode("\r\n", $rulesToWrite));

	$adminMenu = function (){
		return array(__('Plugins') => '/admin/install');
	};
	on('admin/menu', $adminMenu);

	route('/admin', function (){
		var_set('webpage/title', __('Dashboard'));

		print title(__('Dashboard'), 2);
		$mainMenu = trigger('admin/menu');
		if( $mainMenu )
			print menu($mainMenu, ['class' => 'menu-top']);
	});
	
	route('/admin/install', function () use(&$json) {

		var_set('webpage/title', __('Plugins'));

		$path = dirname(__FILE__).'/../config.php';
		$config = var_get('config');
		
		$plugins = glob('plugins/*.php');
		foreach( $plugins as $plugin ){
			if( $plugin == 'plugins/core.php') continue;
			require_once($plugin);
		}
		$info = trigger('plugin');

/*
		for($i = 0; $i < sizeof($info['id']); ++$i ){
			if( isset($info['requirements'][$i]) ){
				var_dump($info['requirements'][$i], $info['id'][$i]);
				foreach( $info['requirements'][$i] as $pr){
					if( !in_array($pr, $_REQUEST['plugins']) )
						$_REQUEST['plugins'][] = $pr;
				}
			}
		}*/
		$formInstall = new Form();
		$formInstall->btnLabel = __('Install');

		if( $formInstall->validate($_REQUEST) ){
			if( !isset($_REQUEST['plugins']) )
				$_REQUEST['plugins'] = array();
			if( !in_array('core', $_REQUEST['plugins']) )
				$_REQUEST['plugins'][] = 'core';

			$plugins_uninstall = array_diff($config['core']['plugins'], $_REQUEST['plugins']);
			$globs = glob('plugins/*.php');
			foreach( $globs as $glob ){
				$glob = preg_replace('/plugins\/([^\.]+)\.php/ui', '$1', $glob);
				off('plugin');
				require('plugins/'.$glob.'.php');
				$dependsOpt = trigger('plugin');
				foreach( $plugins_uninstall as $p ){
					if( isset($dependsOpt['requirements']) && in_array($p, $dependsOpt['requirements'])){
						$plugins_uninstall[] = $dependsOpt['id'];
					}
				}
			}

			off('plugin_uninstall');
			$uninstallFunc = function ($p) use (&$config, &$uninstallFunc){
				off('plugin');
				require('plugins/'.$p.'.php');
				$opts = trigger('plugin');
				unset($config['core']['plugins'][array_search($p, $config['core']['plugins'])]);
				unset($config[$p]);
				unset($_REQUEST['plugins'][array_search($p, $_REQUEST['plugins'])]);
			};
			foreach ($plugins_uninstall as $p){
				// Uninstall plugins
				$uninstallFunc($p);
			}
			trigger('plugin_uninstall');
				
			off('plugin_install');
			$plugins_install = array_diff($_REQUEST['plugins'], $config['core']['plugins']);
			$installFunc = function($p) use( &$config, &$installFunc ) {
				if( in_array($p, $config['core']['plugins']))
					return;
				//off('plugin_install');
				off('plugin');
				require('plugins/'.$p.'.php');
				//trigger('plugin_install');
				$opts = trigger('plugin');
				if (!isset($config[$p]) ) 
					$config[$p] = $opts['options'];
				// Install requirements
				if( isset($opts['requirements']) ){
					foreach( $opts['requirements'] as $pr){
						$installFunc($pr);
					}
				}
				if( !in_array($p, $config['core']['plugins']))
					$config['core']['plugins'][] = $p;
			};
			foreach ($plugins_install as $p)
				$installFunc($p);

			trigger('plugin_install');

			/*if( isset($_REQUEST['plugins'])){
				foreach( $_REQUEST['plugins'] as $p ){
					$config['core']['plugins'][] = $p;
				}
				$config['core']['plugins'] = array_unique($config['core']['plugins']);
			}*/
			file_put_contents($path, '<?php return json_decode(\''.json_encode($config, JSON_PRETTY_PRINT).'\', true); ?>');
			redirect('/admin/install');
		}

		for ($i=0; $i < sizeof($info['id']); $i++) { 
			$formInstall->children[$info['id'][$i]] = new BooleanField(array(
				'name' => 'plugins',
				'multiple' => true,
				'label' => __($info['name'][$i]),
				'value' => $info['id'][$i],
				'checked' => isset($_REQUEST['plugins']) ? in_array($info['id'][$i], $_REQUEST['plugins']) : in_array($info['id'][$i], $config['core']['plugins'])
			));
			if( $info['id'][$i] == 'core' ){
				$formInstall->children[$info['id'][$i]]->attributes['disabled'] = true;
				$formInstall->children[$info['id'][$i]]->attributes['checked'] = true;
			}
			/*else
				$formInstall->children[$info['id'][$i]]->attributes['checked'] = in_array($info['id'][$i], $json['plugins']);*/
			$formInstall->children[$info['id'][$i]]->after = '<p>'.__($info['description'][$i]).'</p>';
		}

		print title(__('Plugins'), 2);
		print $formInstall->render();
		print '<p><a href="/admin"><a href="/admin">'.__('Dashboard').'</a></p>';

	});

};
on('plugin_load', $plugin_load, PHP_INT_MAX);