<?php
plugin_require(array('sql', 'field', 'model'));

load_translations(array('translations' => array(
	'Datas' => array('fr' => 'Données'),
	'EntriesPerPage' => array('fr' => 'Entrées par page', 'en' => 'Entries per page'),
	'NewAddress' => array('fr' => 'Nouvelle adresse', 'en' => 'New address'),
	'InvalidAddress' => array('fr' => 'Adresse invalide.', 'en' => 'Invalid address.'),
	'MyAddresses' => array('fr' => 'Adresses enregistrées', 'en' => 'My addresses'),
	'StreetAddress' => array('fr' => 'Adresse', 'en' => 'Street address'),
	'AddressLocality' => array('fr' => 'Ville', 'en' => 'City'),
	'AddressRegion' => array('fr' => 'Département / Région', 'en' => 'Region'),
	'AddressCountry' => array('fr' => 'Pays', 'en' => 'Country'),
	'PostalCode' => array('fr' => 'Code postal'),
	'GivenName' => array('fr' => 'Prénom', 'en' => 'Given name'),
	'FamilyName' => array('fr' => 'Nom', 'en' => 'Family name'),
	'Gender' => array('fr' => 'Sexe'),
	'MaleCivility' => array('fr' => 'Monsieur'),
	'FemaleCivility' => array('fr' => 'Madame'),
	'DatasPluginDesc' => array(
		'fr' => 'Met à disposition les données de site web',
		'en' => 'Permits to load website datas'
	)
)));
$plugin = function () { return array(
		'id' => 'datas',
		'name' => 'Datas',
		'description' => 'DatasPluginDesc',
		'options' => array(
			'host' => 'localhost',
			'db' => '',
			'port' => 3306,
			'user' => 'root',
			'pass' => ''
		)
	);
};
on('plugin', $plugin);

$plugin_install = function (){

	$path = dirname(__FILE__).'/../datas.json';
	if( !file_exists($path) )
		$schema = array();
	else
		$schema = json_load($path);
	$schema = array_merge($schema, array(
		array(
			'name' => 'media',
			'columns' => array(
				'parent INT(11)',
				'name VARCHAR(255)',
				'url TEXT'
			),
			'stringKey' => 'name',
			'foreignKeys' => array('parent' => 'media(id) ON DELETE SET NULL')
		),
		array(
			'name' => 'contact_point',
			'columns' => array(
				'address_country VARCHAR(255)',
				'address_region VARCHAR(255)',
				'address_locality VARCHAR(255)',
				'postal_code VARCHAR(10)',
				'street_address VARCHAR(255)',
				'contact_type VARCHAR(255)',
				'email VARCHAR(255)',
				'telephone VARCHAR(255)',
				'faxNumber VARCHAR(255)'
			),
			'stringKey' => 'street_address',
			'uniqueKeys' => array('email')
		),
		array(
			'name' => 'organization',
			'columns' => array(
				'parent_organization INT(11)',
				'name VARCHAR(255)',
				'description TEXT',
				'image INT(11)',
				'url VARCHAR(255)',
				'address INT(11)' 
			),
			'stringKey' => 'name',
			'uniqueKeys' => array('name'),
			'foreignKeys' => array(
				'parent_organization' => 'organization(id) ON DELETE SET NULL',
				'address' => 'contact_point(id) ON DELETE SET NULL',
				'image' => 'media(id) ON DELETE SET NULL'
			)
		),
		array(
			'name' => 'person',
			'columns' => array(
				'birth_date DATETIME',
				'birth_place VARCHAR(255)',
				'given_name VARCHAR(255)',
				'family_name VARCHAR(255)',
				'gender VARCHAR(10)',
				'description TEXT',
				'image INT(11)',
				'name VARCHAR(255)',
				'url VARCHAR(255)',
			),
			'stringKey' => 'name',
			'foreignKeys' => array('image' => 'media(id) ON DELETE SET NULL')
		),
		array(
			'name' => 'person_contact',
			'columns' => array(
				'person_id INT(11)',
				'contact_name VARCHAR(255)',
				'contact_id INT(11)'
			),
			'stringKey' => 'contact_name',
			'foreignKeys' => array(
				'person_id' => 'person(id) ON DELETE CASCADE',
				'contact_id' => 'contact_point(id) ON DELETE CASCADE'
			)
		),
		array(
			'name' => 'content',
			'columns' => array(
				'title VARCHAR(255)',
				'content LONGTEXT',
				'template VARCHAR(255)',
				'created_at DATETIME DEFAULT NOW()',
				'updated_at DATETIME DEFAULT NOW()'
			),
			'stringKey' => 'title'
		),
		array(
			'name' => 'website',
			'columns' => array(
				'url VARCHAR(255)',
				'title VARCHAR(255)',
				'author INT(11)',
				'media INT(11)',
				'generator VARCHAR(255)',
				'description VARCHAR(255)',
				'keywords VARCHAR(255)'
			),
			'stringKey' => 'url',
			'uniqueKeys' => array('url'),
			'foreignKeys' => array('media' => 'media(id) ON DELETE SET NULL')
		),
		array(
			'name' => 'webpage',
			'columns' => array(
				'id_website INT(11)',
				'route VARCHAR(255)',
				'content INT(11)',
				'template VARCHAR(255)',
				'description VARCHAR(255)',
			),
			'stringKey' => 'title',
			'foreignKeys' => array(
				'id_website' => 'website(id) ON DELETE CASCADE',
				'content' => 'content(id) ON DELETE SET NULL'
			)
		)
	));
	json_save($path, $schema, true);
};
on('plugin_install', $plugin_install, PHP_INT_MAX-100);

$plugin_uninstall = function (){
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		foreach( $schema as $k => $v){
			if( in_array($v['name'], array('contact_point', 'organization', 'media', 'person', 'person_contact', 'website', 'webpage', 'content')) )
				unset($schema[$k]);
		}
		json_save($path, $schema, true);
	}
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_load = function (){
	$opts = var_get('config/datas');
	if( $opts['db'] == ''){
		die('Change "db" options in the config file');
	}
	sql_connect($opts);

	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) )
		$schema = json_load($path);
	else
		$schema = array();

	foreach( $schema as $table ){
		if( !sql_table_exists($table['name']) ){
			sql_create_table($table);
		}else{
			/*var_set('sql/dump', true);
			foreach( $table['columns'] as $col ){
				sql_update_column($table['name'], $col);
			}*/
		}
		route('/admin/datas/'.$table['name'], function () use (&$schema, &$table){

			var_set('webpage/title', __('Datas') . ' : ' . $table['name']);
			//var_set('sql/dump', true);
			if( isset($_REQUEST['delete']) && is_numeric($_REQUEST['delete'])){
				sql_delete($table['name'], array('where' => 'id='.(int)$_REQUEST['delete']));
			}
			$rawColumns = sql_columns($table['name']);

			$rawKeys = sql_get_fks($table['name']);

			$rawKey = function ($column) use (&$rawKeys) {
				foreach( $rawKeys as $rk ){
					if ($rk['COLUMN_NAME'] == $column){
						return $rk;
					}
				}
				return null;
			};
			$stringKey = function($name) use (&$schema) {
				foreach( $schema as $table ){
					if( $table['name'] == $name ){
						return $table['stringKey'];
					}
				}
				return null;
			};

			$script = '';
			$tbl = array('head' => array());
			$form = new Form();

			$formRenderFunc = function () use ($form){
				$ckeditor = new stdclass();
				$ckeditor->src = 'https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js';
				$ckeditor->type = 'text/javascript';
				return array($ckeditor);
			};
			on('webpage/javascript', $formRenderFunc);

			$insert_type = isset($_REQUEST['edit']) && is_numeric($_REQUEST['edit']) ? 'edit' : 'insert';
			$form->name = $insert_type;
			$newOne = array('actions' => '<input type="submit" name="btnSearch" value="'.__('Search').'" />'.($insert_type == 'insert' ? '<input type="submit" name="btnInsert" value="Insert" /><input type="reset" name="btnReset" value="Reset" />' : '<input type="submit" name="btnEdit" value="Edit" />'));
			$cols = array();
			if( $insert_type == 'edit' )
				$edit_data = sql_get($table['name'], array('limit' => 1, 'where' => 'id='.sql_quote($_REQUEST['edit'])));
			else
				$edit_data = null;
			foreach( $rawColumns as $col ){
				$cols[] = $col['COLUMN_NAME'];
				$tbl['head'][$col['COLUMN_NAME']] = $col['COLUMN_NAME'];
				$field = null;
				if( $col['COLUMN_KEY'] == 'MUL') {
					$rkey = $rawKey($col['COLUMN_NAME']);
					$datas = sql_get($rkey['REFERENCED_TABLE_NAME']);
					$ormData = array();
					if( $datas ){
						$ormData['NULL'] = '---';
						foreach( $datas as $data ){
							$strKey = $stringKey($rkey['REFERENCED_TABLE_NAME']);
							$ormData[$data[$rkey['REFERENCED_COLUMN_NAME']]] = ( $strKey ) ? $data[$strKey] : $data[$rkey['REFERENCED_COLUMN_NAME']];
						}
					}
					$field = new RelationField(array('name' => $col['COLUMN_NAME'], 'datas' => $ormData, 'label_position' => 'none'));
				}else {
					$triggeredField = trigger('datas/'.$table['name'].'/'.$col['COLUMN_NAME']);
					if( $triggeredField ){
						$field = $triggeredField;
					}else{
						switch( $col['DATA_TYPE'] ) {
							case 'int':
								$field = new NumberField(array('name' => $col['COLUMN_NAME'], 'label_position' => 'none'));
							break;
							case 'float':
								$field = new NumberField(array('name' => $col['COLUMN_NAME'], 'label_position' => 'none', 'step' => 0.01));
							break;
							case 'tinytext':
							case 'varchar':
								$field = new TextField(array('name' => $col['COLUMN_NAME'], 'label_position' => 'none'));
							break;
							case 'datetime':
								$field = new TextField(array('name' => $col['COLUMN_NAME'], 'placeholder' => date('Y-m-d H:i:s'), 'label_position' => 'none'));
							break;
							case 'text':
								$field = new TextAreaField(array('name' => $col['COLUMN_NAME'], 'label_position' => 'none'));
							break;
							case 'longtext':
								$field = new TextAreaField(array('name' => $col['COLUMN_NAME'], 'label_position' => 'none'));
								$script .= javascript(array(), 'CKEDITOR.replace("'.$field->attributes['id'].'", {
							      toolbarGroups: [{
						          "name": "basicstyles",
						          "groups": ["basicstyles"]
						        },
						        {
						          "name": "links",
						          "groups": ["links"]
						        },
						        {
						          "name": "paragraph",
						          "groups": ["list", "blocks"]
						        },
						        {
						          "name": "document",
						          "groups": ["mode"]
						        },
						        {
						          "name": "insert",
						          "groups": ["insert"]
						        },
						        {
						          "name": "styles",
						          "groups": ["styles"]
						        },
						        {
						          "name": "about",
						          "groups": ["about"]
						        }
						      ],
									removeButtons: "Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar"
								});');
							break;
							default:
								if( $col['COLUMN_TYPE'] == 'tinyint(1)')
									$field = new BooleanField(array('name' => $col['COLUMN_NAME'], 'label_position' => 'none'));
							break;
						}
					}
				}
				$form->children[] = $field;
				if( $insert_type == 'edit'){
					$field->attributes['value'] = ''.$edit_data[$col['COLUMN_NAME']];
				}
				$newOne[$col['COLUMN_NAME']] = $field;
			}

			var_set('entriesPerPage', array(10 => "10", 50 => "50", 100 => "100"));
			$entries = var_get('entriesPerPage');
			if( !isset($_REQUEST['limit']) )
				$_REQUEST['limit'] = (int)(current($entries));
			if( !isset($_REQUEST['page']) )
				$_REQUEST['page'] = 1;

			if( isset($_REQUEST['id']) ){
				$all_data_count = 1;
				$all_data = sql_get($table['name'], array('where' => 'id='.sql_quote($_REQUEST['id'])));
			}
			else{
				$all_data_count = sql_query('SELECT COUNT(*) as cnt FROM '.var_get('sql/options/db').'.'.$table['name']);
				$all_data_count = $all_data_count['cnt'];
				$all_data = sql_get($table['name'], array('limit' => $_REQUEST['limit'], 'offset' => ($_REQUEST['page']-1)*$_REQUEST['limit']));
			}

			if( isset($_REQUEST['btnInsert']) || isset($_REQUEST['btnEdit']) ){
				$to_insert = array_filter($_REQUEST, function ($key) use ($cols) {
					return in_array($key, $cols);
				}, ARRAY_FILTER_USE_KEY);

				foreach( $rawColumns as $col ){
					if( $col['COLUMN_KEY'] == 'MUL' && $_REQUEST[$col['COLUMN_NAME']] == 'NULL') {
						$to_insert[$col['COLUMN_NAME']] = null;
					}
					if( $col['DATA_TYPE'] == 'datetime' && DateTime::createFromFormat('Y-m-d H:i:s', $_REQUEST[$col['COLUMN_NAME']]) == false ){
						unset($to_insert[$col['COLUMN_NAME']]);
					}
					if( $col['COLUMN_TYPE'] == 'tinyint(1)'){
						$to_insert[$col['COLUMN_NAME']] = $to_insert[$col['COLUMN_NAME']] == 'on' ? 1 : 0;
					}
				}

				if( $insert_type == 'edit')
					sql_update($table['name'], $to_insert, 'id='.$_REQUEST['edit']);
				else
					sql_insert($table['name'], $to_insert);

				redirect('/admin/datas/'.$table['name'].'?page='.$_REQUEST['page']);

			}else if( isset($_REQUEST['btnSearch']) ){
				$_REQUEST['page'] = 1;
				$where = array();
				foreach( $rawColumns as $col ){

					if( $col['COLUMN_KEY'] == 'MUL'){
						if( isset($_REQUEST[$col['COLUMN_NAME']]) ){
							$val = $_REQUEST[$col['COLUMN_NAME']];
							if( $_REQUEST[$col['COLUMN_NAME']] == 'NULL')
								$val = null;
						}
						if( $val ){
							$where[] = $col['COLUMN_NAME'].'='.sql_quote($val);
						}
					}else{
						switch ( $col['DATA_TYPE'] == 'varchar'){
							case 'varchar':
							case 'text':
								$val = isset($_REQUEST[$col['COLUMN_NAME']]) ? $_REQUEST[$col['COLUMN_NAME']] : null;
								if( $val )
									$where[] = $col['COLUMN_NAME'].' LIKE '.sql_quote('%'.$val.'%');
							break;
							default:
								$val = isset($_REQUEST[$col['COLUMN_NAME']]) ? $_REQUEST[$col['COLUMN_NAME']] : null;
								if( $val )
									$where[] = $col['COLUMN_NAME'].'='.sql_quote($val);
							break;
						}
					}
				}					
	//			var_set('sql/dump', true);
				$all_data_count = sql_query('SELECT COUNT(*) as cnt FROM '.var_get('sql/options/db').'.'.$table['name'].(sizeof($where) ? ' WHERE '.implode(' AND ', $where) : ''));
				$all_data_count = $all_data_count['cnt'];
				$all_data = sql_get($table['name'], array('where' => implode(' AND ', $where), 'limit' => $_REQUEST['limit'], 'offset' => ($_REQUEST['page']-1)*$_REQUEST['limit']));
			}

			$tbl['head']['actions'] = __('Actions');

			$tbl['body'] = $all_data;
			if( $tbl['body'] ){
				$related_data = array();
				$getRefValue = function($datas, $id, $column) {
					if( !$datas )
						return null;
					foreach( $datas as $d ){
						if( $d['id'] == $id)
							return $d[$column];
					}
					return null;
				};
				$fmt = new IntlDateFormatter(current_locale(), IntlDateFormatter::LONG, IntlDateFormatter::SHORT);
				foreach( $tbl['body'] as &$entry ){
					foreach( $rawColumns as $col ){
						if( $col['COLUMN_KEY'] == 'MUL'){
							$rkey = $rawKey($col['COLUMN_NAME']);
							if( !isset($related_data[$rkey['REFERENCED_TABLE_NAME']]) ){
								$rd = sql_get($rkey['REFERENCED_TABLE_NAME']);
								$related_data[$rkey['REFERENCED_TABLE_NAME']] = $rd;
							}
							$refId = $entry[$col['COLUMN_NAME']];
							$refData = $getRefValue($related_data[$rkey['REFERENCED_TABLE_NAME']], $entry[$col['COLUMN_NAME']], $stringKey($rkey['REFERENCED_TABLE_NAME']));
							if( !is_null($refData) )
								$entry[$col['COLUMN_NAME']] = '<a href="/admin/datas/'.$rkey['REFERENCED_TABLE_NAME'].'?id='.$refId.'">'.htmlspecialchars($refData).'</a>';
						}else {
							switch( $col['DATA_TYPE'] ){
								case 'datetime':
									$dt = new DateTime($entry[$col['COLUMN_NAME']]);
									$entry[$col['COLUMN_NAME']] = $fmt->format($dt->getTimestamp());
								break;
								case 'text':
									$entry[$col['COLUMN_NAME']] = substr($entry[$col['COLUMN_NAME']], 0, 255);
								break;
								case 'longtext':
									$entry[$col['COLUMN_NAME']] = substr(strip_tags($entry[$col['COLUMN_NAME']]), 0, 255);
								break;
								default:
								break;
							}
						}
					}
					$entry['actions'] = '<a href="/admin/datas/'.$table['name'].'?edit='.$entry['id'].'">Edit</a> | <a href="/admin/datas/'.$table['name'].'?delete='.$entry['id'].'">Delete</a>';
				}
			}
			$tbl['body'][] = $newOne;

			print title(__('Datas').' : '.__($table['name']), 2);

			print $form->openForm();

			print table($tbl);
			
			$limitPerPage = new SelectField(array('name' => 'limit', 'label' => __('EntriesPerPage'), 'datas' => var_get('entriesPerPage')));

			$page_max = (int)ceil($all_data_count / $_REQUEST['limit']);
			//var_dump($all_data_count, $_REQUEST['limit'], $page_max);
			if( $page_max > 1 ){
				$pagination = array();
				if( $_REQUEST['page'] != 1 )
					$pagination[] = '<a href="/admin/datas/'.$table['name'].'?page=1">&lt;&lt;</a>';
				if( $_REQUEST['page'] > 1 )
					$pagination[] = '<a href="/admin/datas/'.$table['name'].'?page='.($_REQUEST['page']-1).'">&lt;</a>';
				$pagination[] = '<b>'.$_REQUEST['page'].'</b>';
				if( $_REQUEST['page'] < $page_max )
					$pagination[] = '<a href="/admin/datas/'.$table['name'].'?page='.($_REQUEST['page']+1).'">&gt;</a>';
				if( $_REQUEST['page'] != $page_max )
					$pagination[] = '<a href="/admin/datas/'.$table['name'].'?page='.$page_max.'">&gt;&gt;</a>';
				print '<div class="pagination">'.implode(' ', $pagination).'</div>';
			}
			print $limitPerPage->render();

			print $form->closeForm();
			print menu(array(
				__('Datas') => '/admin/datas',
				__('Dashboard') => '/admin'
			));

			print $script;
		});
	}

	route('/admin/datas', function () use($schema) {
		var_set('webpage/title', __('Datas'));
		print title(__('Datas'), 2);
		$html = '<ul>';
		usort($schema, function($a, $b) {
			return $a['name']>$b['name'] ? 1 : -1;
		});
		foreach( $schema as $table ){
			$html .= '<li><a href="/admin/datas/'.$table['name'].'">'.$table['name'].'</a></li>';
		}
		$html .= '</ul>';
		$html .= '<p><a href="/admin">'.__('Dashboard').'</a></p>';
		print $html;
	});

	$menuElements = function (){
		return array(__('Datas') => '/admin/datas');
	};
	on('admin/menu', $menuElements);

/*
	$sqlLines = function(){

		sql_insert('website', array('url' => pts\URI::root()));
		var_set('website/id', $wid = sql_last_id());

		sql_insert('webpage', array('id_website' => $wid, ))
	};
	on('datas/init', $sqlLines);

	trigger('datas/init');
	//*/
};
on('plugin_load', $plugin_load);
