<?php
plugin_require(array('sql', 'field', 'model'));

load_translations(array('translations' => array(
	'Cart' => array('fr' => 'Panier'),
	'ProductName' => array(
		'fr' => 'Nom du produit',
		'en' => 'Product name'
	),
	'Quantity' => array(
		'fr' => 'Quantité',
		'en' => 'Quantity'
	),
	'Price' => array(
		'fr' => 'Prix',
		'en' => 'Price'
	),
	'TotalPrice' => array(
		'fr' => 'Prix total',
		'en' => 'Total price'
	),
	'DiscountCode' => array(
		'fr' => 'Code promotionnel',
		'en' => 'Discount code'
	),
	'Delivery' => array(
		'fr' => 'Livraison',
	),
	'DeliveryCountry' => array(
		'fr' => 'Pays de la livraison',
		'en' => 'Delivery country'
	),
	'DeliveryAddress' => array(
		'fr' => 'Adresse de livraison',
		'en' => 'Delivery address'
	),
	'DeliveryOptions' => array(
		'fr' => 'Options de livraison',
		'en' => 'Delivery options'
	),
	'ShippingFees' => array(
		'fr' => 'Frais de port',
		'en' => 'Shipping fees'
	),
	'PaymentMethods' => array(
		'fr' => 'Méthodes de paiement',
		'en' => 'Payment methods'
	),
	'AddToCart' => array(
		'fr' => 'Ajouter au panier',
		'en' => 'Add to cart'
	),
	'ProductsInCart' => array(
		'fr' => 'Produits dans votre panier',
		'en' => 'Products in your cart'
	),
	'NoProductInCart' => array(
		'fr' => 'Aucun produit n\'a été ajouté à votre panier.',
		'en' => 'No product in your cart.'
	),
	'UnavailableProduct' => array(
		'fr' => 'Le produit n\'est plus en stock',
		'en' => 'Unavailable product'
	),
	'AvailableProduct' => array(
		'fr' => 'Produit en stock',
		'en' => 'Available product'
	),
	'SimilarProducts' => array(
		'fr' => 'Produits similaires',
		'en' => 'Similar products'
	),
	'MyOrders' => array(
		'fr' => 'Mes commandes',
		'en' => 'My orders'
	),
	'OrderCode' => array(
		'fr' => 'Numéro de commande',
		'en' => 'Order ID'
	),
	'OrderDate' => array(
		'fr' => 'Commandé le',
		'en' => 'Ordered at'
	),
	'EShop' => array('fr' => 'E-Commerce', 'en' => 'E-Shopping'),
	'EShopPluginDesc' => array(
		'fr' => 'Transforme votre site internet en site e-commerce.',
		'en' => 'Transforms your website in an e-commerce website.'
	)
)));
$plugin = function () { return array(
		'id' => 'eshop',
		'name' => 'EShop',
		'description' => 'EShopPluginDesc',
		'requirements' => array('datas', 'auth'),
		'options' => array(
			'defaultCurrency' => 'EUR',
			'defaultCountry' => 'FR',
			'paymentMethods' => array('CreditCard', 'Check', 'Deposit')
		)
	);
};
on('plugin', $plugin);

$plugin_install = function (){
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		$schema = array_merge($schema, array(
			array(
				'name' => 'brand',
				'columns' => array(
					'name VARCHAR(255)',
					'url VARCHAR(255)',
					'logo INT(11)',
					'slogan VARCHAR(255)'
				),
				'stringKey' => 'name',
				'foreignKeys' => array('logo' => 'media(id) ON DELETE SET NULL')
			),
			array(
				'name' => 'product_category',
				'columns' => array(
					'parent_category INT(11)',
					'name VARCHAR(255)',
					'media INT(11)',
					'description TEXT'
				),
				'stringKey' => 'name',
				'foreignKeys' => array(
					'media' => 'media(id)',
					'parent_category' => 'product_category(id) ON DELETE SET NULL'
				)
			),
			array(
				'name' => 'product',
				'columns' => array(
					'brand INT(11)',
					'category INT(11)',
					'code VARCHAR(255)',
					'name VARCHAR(255)',
					'media INT(11)',
					'content INT(11)',
					'virtualProduct BOOLEAN DEFAULT FALSE',
					'width FLOAT',
					'height FLOAT',
					'depth FLOAT',
					'weight FLOAT',
					'stock INT(11)',
					'part_of INT(11)',
					'price FLOAT',
					'consumable_for INT(11)',
				),
				'stringKey' => 'name',
				'foreignKeys' => array(
					'brand' => 'brand(id) ON DELETE SET NULL',
					'category' => 'product_category(id) ON DELETE SET NULL',
					'part_of' => 'product(id) ON DELETE SET NULL',
					'consumable_for' => 'product(id) ON DELETE SET NULL',
					'media' => 'media(id) ON DELETE SET NULL',
					'content' => 'content(id) ON DELETE SET NULL'
				)
			),
			array(
				'name' => 'discount',
				'columns' => array(
					'code VARCHAR(255)',
					'apply_until DATETIME',
					'price_value FLOAT',
					'price_percent FLOAT',
				),
				'stringKey' => 'code',
			),
			array(
				'name' => 'delivery_method',
				'columns' => array(
					'name VARCHAR(255)',
					'description TEXT',
					'max_width FLOAT',
					'max_height FLOAT',
					'max_depth FLOAT',
					'max_weight FLOAT',
					'shipping_fees FLOAT',
					'country VARCHAR(5)'
				),
				'stringKey' => 'name'
			),
			array(
				'name' => 'order',
				'columns' => array(
					'account_id INT(11)',
					'order_code VARCHAR(50)',
					'order_date DATETIME',
					'order_status INT(4)',
					'price FLOAT',
					'discount_id INT(11)',
					'payment_method VARCHAR(255)',
					'billing_address INT(11)',
					'delivery_method INT(11)',
					'delivery_address INT(11)',
					'delivery_origin_address INT(11)',
					'delivery_status TEXT',
					'expected_arrival DATETIME',
					'expected_arrival_until DATETIME',
					'tracking_number VARCHAR(255)',
					'tracking_url TEXT'
				),
				'stringKey' => 'order_code',
				'foreignKeys' => array(
					'account_id' => 'account(id) ON DELETE CASCADE',
					'billing_address' => 'contact_point(id) ON DELETE SET NULL',
					'discount_id' => 'discount(id) ON DELETE SET NULL',
					'delivery_method' => 'delivery_method(id) ON DELETE SET NULL',
					'delivery_address' => 'contact_point(id) ON DELETE SET NULL',
					'delivery_origin_address' => 'contact_point(id) ON DELETE SET NULL'
				)
			),
			array(
				'name' => 'order_product',
				'columns' => array(
					'order_id INT(11)',
					'product_id INT(11)',
					'quantity INT(11)',
				),
				'stringKey' => 'id',
				'foreignKeys' => array(
					'order_id' => 'order(id) ON DELETE CASCADE',
					'product_id' => 'product(id)'
				)
			)
		));
		json_save($path, $schema, true);
	}

};
on('plugin_install', $plugin_install, 1000);

$plugin_uninstall = function (){
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		foreach( $schema as $k => $v){
			if( in_array($v['name'], array('brand', 'product', 'product_category', 'offer', 'discount', 'order', 'order_product', 'delivery_method')) )
				unset($schema[$k]);
		}
		json_save($path, $schema, true);
	}
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_load = function (){

	$rules = function(){
		return array(
			'RewriteRule ^cat/([0-9]+)\-([^\.]+)\.html$ /category?cid=$1 [L]',
			'RewriteRule ^prd/([0-9]+)\-([^\.]+)\.html$ /product?pid=$1 [L]'
		);
	};
	on('core/htaccess', $rules, 10);	

	$frontStyles = function (){
		$style = new stdclass();
		$style->href = 'app/css/eshop.css?v='.uniqid();
		$style->type = 'text/css';
		$style->rel = 'stylesheet';
		return array($style);
	};
	on('webpage/styles', $frontStyles);

};
on('plugin_load', $plugin_load);

if( !function_exists('get_currency_symbol') ){
	function get_currency_symbol(){
		$c = var_get('config/eshop/defaultCurrency', 'EUR');
		if( $c == 'EUR' )
			return '€';
		return '€';
	}
}

if( !function_exists('eshop_order_pdf') ){
	function eshop_order_pdf($options=array()){

		$options = array_replace(array(
			'creator' => 'PHP Tool Suite'
		), $options);

		$p = new PDFlib();

		if( $p->begin_document("", "") == 0 ){
			die('Error:' . $p->get_errmsg());
		}

		$p->set_info('Creator', $options['creator']);
		if( isset($options['author']) )
			$p->set_info('Author', $options['author']);
		if( isset($options['title']) )
			$p->set_info('Title', $options['title']);

		$p->begin_page_ext(595, 842, ""); // A4

		$font = $p->load_font("Helvetica-Bold", "winansi", "");
		$p->setfont($font, 24.0);

		$p->end_page_ext("");

    $p->end_document("");

    $buf = $p->get_buffer();
    $len = strlen($buf);

    header("Content-type: application/pdf");
    header("Content-Length: ".$len);
    header("Content-Disposition: inline; filename=".$options['filename']);
    print $buf;
	}
}