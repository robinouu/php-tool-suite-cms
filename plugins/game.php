<?php
load_translations(array('translations' => array(
	'GamePluginDesc' => array(
		'fr' => 'Plugin pour la création d\'un jeu vidéo',
		'en' => 'Game plugin.'
	)
)));
$plugin = function (){
	return array(
		'id' => 'game',
		'name' => 'Game',
		'description' => 'GamePluginDesc',
		'requirements' => array('datas'),
		'options' => array(
		)
	);
};
on('plugin', $plugin);

$plugin_install = function (){
	
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		$schema = array_merge($schema, array(
			array(
				'name' => 'gamer',
				'columns' => array(
					'account INT(11)'
				),
				'foreignKeys' => array(
					'account' => 'account(id)'
				),
				'stringKey' => null
			)
		));
		json_save($path, $schema, true);
	}

};
on('plugin_install', $plugin_install);

$plugin_uninstall = function (){
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		foreach( $schema as $k => $v){
			if( in_array($v['name'], array('gamer')) )
				unset($schema[$k]);
		}
		json_save($path, $schema, true);
	}
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_load = function (){
  
	$frontStyles = function (){
		$style = new stdclass();
		$style->href = 'app/css/game.css?uid='.uniqid();
		$style->type = 'text/css';
		$style->rel = 'stylesheet';
		return array($style);
	};
	on('webpage/styles', $frontStyles);

};
on('plugin_load', $plugin_load);