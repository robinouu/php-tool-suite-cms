<?php

load_translations(array('translations' => array(
	'CurrentLocale' => array(
		'fr' => 'Langue',
		'en' => 'Current locale'
	),
	'CurrentTimezone' => array(
		'fr' => 'Fuseau horaire',
		'en' => 'Current timezone'
	),
	'SupportedLanguages' => array(
		'fr' => 'Langues supportées',
		'en' => 'Supported languages'
	),
	'I18n' => array(
		'fr' => 'Internationalisation',
		'en' => 'Internationalisation'
	),
	'I18nPluginDesc' => array(
		'fr' => 'Ajoute les fonctionnalités de langues et traductions',
		'en' => 'Adds lang and translations features'
	)
)));

$plugin = function (){
	return array(
		'id' => 'i18n',
		'name' => 'I18n',
		'description' => 'I18nPluginDesc',
		'options' => array(
			'langs' => array('fr', 'en'),
			'locale' => 'fr_FR',
			'timezone' => 'Europe/Paris'
		)
	);
};
on('plugin', $plugin);

$plugin_uninstall = function (){
	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		foreach( $schema as $k => $v){
			if( in_array($v['name'], array('content_version')) )
				unset($schema[$k]);
		}
		json_save($path, $schema, true);
	}
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_install = function (){

	$path = dirname(__FILE__).'/../datas.json';
	if( file_exists($path) ){
		$schema = json_load($path);
		$schema = array_merge($schema, array(
			array(
				'name' => 'content_version',
				'columns' => array(
					'lang VARCHAR(10)',
					'title VARCHAR(255)',
					'content TEXT',
					'template VARCHAR(255)',
					'created_at DATETIME',
					'updated_at DATETIME'
				),
				'stringKey' => 'title'
			)
		));
		json_save($path, $schema, true);
	}

};
on('plugin_install', $plugin_install);

$plugin_load = function (){
	$opts = var_get('config/i18n');

	if( isset($opts['timezone']) )
		date_default_timezone_set($opts['timezone']);
	if( isset($opts['locale']) )
		set_locale($opts['locale']);
	else
		set_locale(preferred_locale());
	
	$menuElements = function (){
		return array(__('I18n') => '/admin/i18n');
	};
	on('admin/menu', $menuElements);
	
	route('/admin/i18n', function () use(&$opts) {
		var_set('webpage/title', __('I18n'));
		print title(__('I18n'), 1);
		print __('CurrentLocale') . ' : ' . current_locale() . br();
		print __('CurrentTimezone') . ' : ' . $opts['timezone'] . br();
		print __('SupportedLanguages').' : '.implode(', ', $opts['langs']);
		print hr();
	});
};
on('plugin_load', $plugin_load);

$plugin_load_trs = function () {
	$opts = var_get('config/i18n');

	$path = dirname(__FILE__).'/../i18n.json';
	if( file_exists($path) ){
		load_translations(array('translations' => json_load($path)));	
	}
	
	$trs = var_get('i18n/translationData');
	$trs = $trs->versions;

	route('/admin/i18n', function () use(&$opts, &$trs, $path) {
		$table = array(
			'head' => array('ID' => 'ID'),
			'body' => array()
		);
		foreach( $opts['langs'] as $lang ){
			$table['head'][$lang] = $lang;
		}
		$table['head']['actions'] = __('Actions');
		foreach( $trs as $k => $v ){
			if( isset($_REQUEST['edit']) && $_REQUEST['edit'] == $k ){
				$res = array();
				foreach( $opts['langs'] as $lang){
					$res[$lang] = new TextField(array('name' => 'translation_'.$lang, 'label_position' => 'none', 'placeholder' => __('Enter traduction'), 'value' => isset($v[$lang]) ? $v[$lang] : ''));
				}
				$b = array_merge(array('ID' => $k), $res);
				$b['actions'] = '<input type="submit" name="btnTranslate" value="'.__('Translate').'" />';
			}else{
				$b = array_merge(array('ID' => $k), $v);
				$b['actions'] = '<a href="/admin/i18n?edit='.$k.'">'.__('Edit').'</a>';
			}
			$table['body'][] = $b;
		}
		if( isset($_REQUEST['edit']) ){
			$form = new Form();
			$form->name = 'translate';
			print $form->openForm();
			print table($table, array('class' => 'table-translation'));
			print $form->closeForm();
			if( $form->validate($_REQUEST) ){
				if( file_exists($path) ){
					$i18nData = json_load($path);
				}else{
					$i18nData = array();
				}
				$data = array();
				foreach( $opts['langs'] as $lang){
					if( isset($_REQUEST['translation_'.$lang])){
						$data[$lang] = $_REQUEST['translation_'.$lang];
					}
				}
				$i18nData[$_REQUEST['edit']] = $data;
				json_save($path, $i18nData, true);
				redirect('/admin/i18n');
			}
		}else{
			print table($table, array('class' => 'table-translation'));
		}
	});
};
on('plugin_load', $plugin_load_trs, PHP_INT_MAX-100);