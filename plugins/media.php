<?php
load_translations(array('translations' => array(
	'AddFile' => array(
		'fr' => 'Ajout de fichier',
		'en' => 'Add file'
	),
	'AddDirectory' => array(
		'fr' => 'Ajout de dossier',
		'en' => 'Add directory'
	),
	'MainDir' => array(
		'fr' => 'Dossier principal',
		'en' => 'Main directory'
	),
	'Medias' => array(
		'fr' => 'Médias',
		'en' => 'Medias'
	),
	'MediaPluginDesc' => array(
		'fr' => 'Bibliothèque de médias (galeries d\'images, fichiers et dossiers)',
		'en' => 'Media library (media galery, files and directories)'
	)
)));
$plugin = function (){
	return array(
		'id' => 'media',
		'name' => 'Medias',
		'description' => 'MediaPluginDesc',
		'requirements' => array('datas'),
		'options' => array(
			'path' => '/medias/'
		)
	);
};
on('plugin', $plugin);

$plugin_load = function (){
	
	$menuElements = function (){
		return array(__('Medias') => '/admin/medias');
	};
	on('admin/menu', $menuElements);
	
	route('/admin/medias', function (){
		var_set('webpage/title', __('Medias'));

		$parents = array();
		if( isset($_REQUEST['id']) ){
			$parent = $_REQUEST['id'];
			$medias = sql_get('media', array('where' => 'parent='.sql_quote($_REQUEST['id'])));
		}
		else{
			$medias = sql_get('media', array('where' => 'parent IS NULL'));
			$parent = null;
		}

		if( isset($_REQUEST['delete']) ){
			$m = sql_get('media', array('where' => 'id='.sql_quote($_REQUEST['delete']), 'limit' => 1));
			if( $m ){
				unlink($p = dirname(__FILE__).'/../'.$m['url']);
				sql_delete('media', array('where' => 'id='.$m['id']));
				redirect('/admin/medias' . ($m['parent'] ? '?id='.$m['parent'] : ''));
			}else{
				redirect('/admin/medias');
			}
		}

		do {
			if( $parent ){
				$req = sql_get('media', array('where' => 'id='.$parent, 'limit' => 1));
				$parent = $req['parent'];
				$parents[] = $req;
			}
		} while( $parent );
		
		print title(__('Medias'), 1);

		$parents = array_reverse($parents);
		$parents_str = array('<a href="/admin/medias">'.__('MainDir').'</a>');
		foreach( $parents as $p ){
			if( $p['id'] == $_REQUEST['id'] ){
				$parents_str[] = htmlspecialchars($p['name']);
			}else{
				$parents_str[] = '<a href="/admin/medias?id='.$p['id'].'">'.htmlspecialchars($p['name']).'</a>';
			}
		}

		print implode(' &gt; ', $parents_str);

		$html = '';
		if( $medias ){
			$html .= '<ul>';
			foreach( $medias as $m ){
				$html .= '<li><a href="/admin/medias?id='.$m['id'].'">'.htmlspecialchars($m['name']).'</a></li>';
			}
			$html .= '</ul>';
		}

		if( isset($_REQUEST['id']) ){
			$media = sql_get('media', array('where' => 'id='.sql_quote($_REQUEST['id']), 'limit' => 1));
			if( $media['url'] )
				$html .= p('<img src="'.$media['url'].'" alt="'.htmlspecialchars($media['name']).'" />');

			if( !$medias )
				$html .= p('<a href="/admin/medias?delete='.$_REQUEST['id'].'">'.__('Delete').'</a>');
		}

		print $html;

		$formAddFile = new Form();
		$formAddFile->name = 'add-file';
		$formAddFile->btnLabel = __('Upload');
		$formAddFile->children['file'] = new FileField(array('name' => 'file', 'label' => __('AddFile'), 'required' => true, 'extensions' => false));
		if( $formAddFile->validate($_REQUEST) ){
			$ext = substr($_FILES['file']['name'], strrpos($_FILES['file']['name'], '.'));

			$filename = microtime().$ext;
			$final_url = var_get('config/media/path').$filename;
			$url = dirname(__FILE__).'/../'.var_get('config/media/path');
			mkdir($url);
			$url = $url.basename($final_url);
			$uploaded = move_uploaded_file($_FILES['file']['tmp_name'], $url);

			sql_insert('media', $m = array(
				'parent' => isset($_REQUEST['id']) ? $_REQUEST['id'] : null,
				'name' => $_FILES['file']['name'],
				'url' => $final_url
			));

			redirect('/admin/medias'.($m['parent'] ? '?id='.$m['parent'] :''));
		}
		print $formAddFile->render();

		$formAddDir = new Form();
		$formAddDir->name = 'add-dir';
		$formAddDir->children['dir'] = new TextField(array('name' => 'dir', 'label' => __('AddDirectory'), 'required' => true));
		if( $formAddDir->validate($_REQUEST) ){
			$m = array('name' => $_REQUEST['dir']);
			if (isset($_REQUEST['id']))
				$m['parent'] = $_REQUEST['id'];
			sql_insert('media', $m);
			redirect('/admin/medias?id='.sql_last_id());
		}
		print $formAddDir->render();

		print menu(array(
			__('Dashboard') => '/admin'
		));
	});
};
on('plugin_load', $plugin_load);