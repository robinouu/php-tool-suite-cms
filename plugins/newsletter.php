<?php
load_translations(array('translations' => array(
	'NewsletterPluginDesc' => array(
		'fr' => 'Plugin pour la publication de newsletters',
		'en' => 'Newsletter publication plugin.'
	),
	'AddAnyTemplateEmail' => array(
		'fr' => 'Veuillez ajouter au moins un template email',
		'en' => 'Add any template email'
	),
	'AddAnyContact' => array(
		'fr' => 'Veuillez ajouter au moins un contact.',
		'en' => 'Add any contact.'
	)
)));
$plugin = function (){
	return array(
		'id' => 'newsletter',
		'name' => 'Newsletter',
		'description' => 'NewsletterPluginDesc',
		'requirements' => array('datas'),
		'options' => array(
			'from' => 'Robin Ruaux <robin.ruaux@gmail.com>',
			'templateDir' => 'app/templates/newsletter'
		)
	);
};
on('plugin', $plugin);

$plugin_install = function (){
	
};
on('plugin_install', $plugin_install);

$plugin_uninstall = function (){
	
};
on('plugin_uninstall', $plugin_uninstall);

$plugin_load = function (){
	
	$menuElements = function (){
		return array(__('Newsletter') => '/admin/newsletter');
	};
	on('admin/menu', $menuElements);
	
	route('/admin/newsletter', function (){

		$templatesPath = dirname(__FILE__).'/../'.var_get('config/newsletter/templateDir').'/';
		$templatesEmail = glob($templatesPath.'*');
		foreach( $templatesEmail as &$tpl ){
			$tpl = basename($tpl);
		}

		$contacts = sql_get('contact_point', array('where' => 'email <> ""'));
		$emailsDatas = array('all' => 'Tous les emails');
		if( $contacts ){
			foreach ($contacts as $v) {
				if( empty($v['email']) )
					continue;
				$emailsDatas[$v['email']] = $v['email'];
			}
		}

		$formSend = new Form();
		$formSend->name = 'send-newsletter';
		$formSend->children['emails'] = new SelectField(array('name' => 'emails', 'label' => __('Emails'), 'datas' => $emailsDatas, 'hasMany' => true));
		$formSend->children['subject'] = new TextField(array('name' => 'subject', 'label' => __('Subject')));
		$formSend->children['template'] = new SelectField(array('name' => 'template', 'label' => __('EmailTemplate'), 'datas' => $templatesEmail));

		if( $formSend->validate($_REQUEST) ){
			plugin_require('mail');
			$templateEmail = $templatesEmail[$_REQUEST['template']];
			
			foreach( $contacts as $contact ){
				if( in_array('all', $_REQUEST['emails']) || in_array($contact['email'], $_REQUEST['emails'])){
					var_set('contact', $contact);
					$templateContent = file_get_contents($templatesPath.$templateEmail);
					email(array(
						'to' => $contact['email'],
						'from' => var_get('config/newsletter/from'),
						'subject' => $_REQUEST['subject'],
						'content' => $templateContent
					));
				}
			}
		}

		var_set('webpage/title', __('Newsletter'));
		print title(__('Newsletter'), 1);

		if( !sizeof($templatesEmail) ){
			print p(__('AddAnyTemplateEmail'));
		}
		else if( !$contacts || !sizeof($contacts) ){
			print p(__('AddAnyContact'));
		}else{
			print $formSend->render();
		}

		print menu(array(
			__('Dashboard') => '/admin'
		));

	});
};
on('plugin_load', $plugin_load);