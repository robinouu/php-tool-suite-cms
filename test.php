<?php

/*
PHP Tool Suite
----

*/
require_once('../ici-sorties/lib/core.inc.php');

$tests = array();
function unitTest($caption, $arguments, $wantedResult, $result) {
	global $tests;
	if( is_array($wantedResult) ) $wantedResult = print_r($wantedResult, true);
	else if( is_bool($wantedResult) ) $wantedResult = $wantedResult ? 'TRUE' : 'FALSE';
	if( is_array($result) ) $result = print_r($result, true);
	else if( is_bool($result) ) $result = $result ? 'TRUE' : 'FALSE';
	$tests[] = array('caption' => $caption, 'arguments' => $arguments, 'wantedResult' => (string)$wantedResult, 'result' => $result);
}
function showTests() {
	global $tests;
	return table(array(
		'caption' => 'Tests unitaires de PHP Tool Suite',
		'head' => $head = array(
			'caption' => 'Fonction',
			'arguments' => 'Arguments',
			'wantedResult' => 'Résultat attendu',
			'result' => 'Résultat'
		),
		'body' => $tests
	));
}


// Arrays (array.inc.php)
plugin_require('array');

unitTest('is_simple_array', '$arr = array(0, 1)', true, is_simple_array(array(0, 1)));
unitTest('is_simple_array', '$arr = array(0 => 1)', true, is_simple_array(array(0 => 1, 1)));
unitTest('is_simple_array', '$arr = array("count" => 1)', false, is_simple_array(array("count" => 1)));
unitTest('is_simple_array', '$arr = array("count" => array())', false, is_simple_array(array("count" => array())));

unitTest('is_assoc_array', '$arr = array(0, 1)', false, is_assoc_array(array(0, 1)));
unitTest('is_assoc_array', '$arr = array(0 => 1, 1)', false, is_assoc_array(array(0 => 1, 1)));
unitTest('is_assoc_array', '$arr = array("count" => 1)', true, is_assoc_array(array("count" => 1)));

plugin_require('random');

set_seed(1);
$arr = array("vars" => 1,"foo" => 2, "bar" => 3, "bar2" => 4, "bar3" => 5);
$arrComp = array("foo" => 2, "bar" => 3, "bar2" => 4, "bar3" => 5, "vars" => 1);

$res = assoc_array_shuffle($arr);
unitTest('assoc_array_shuffle', '$arr = array("vars" => 1,"foo" => 2, "bar" => 3, "bar2" => 4, "bar3" => 5)', true, $res);
unitTest('assoc_array_shuffle', '$arr = array("vars" => 1,"foo" => 2, "bar" => 3, "bar2" => 4, "bar3" => 5)', true, $arr == $arrComp);

unitTest('assoc_array_slice', '$arr = array("vars" => 1,"foo" => 2, "bar" => 3, "bar2" => 4, "bar3" => 5)', $arrComp, assoc_array_slice($arrComp));
unitTest('assoc_array_slice', '$arr = array("vars" => 1,"foo" => 2, "bar" => 3, "bar2" => 4, "bar3" => 5), $start=-1, $len=1', array("vars" => 1), assoc_array_slice($arrComp, -1, 1));
unitTest('assoc_array_slice', '$arr = array("vars" => 1,"foo" => 2, "bar" => 3, "bar2" => 4, "bar3" => 5), $start=1, $len=2', array("bar" => 3, "bar2" => 4), assoc_array_slice($arrComp, 1, 2));
plugin_require('html');



?><!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		tr {
			background-color: gray;
			color: black;
		}
		td {
			padding: 6px;
		}
		tr.green {
			background-color: #4dab49;
			color: black;
		}
	</style>

</head>
<body>

	<?php print showTests(); ?>

	<script type="text/javascript">
		var trs = document.querySelectorAll('tr');
		console.log(trs);
		for (var i = 0; i < trs.length; i++) {
			console.log(trs[i].childNodes[2].innerHTML, trs[i].childNodes[3].innerHTML);
			if( trs[i].childNodes[2].innerHTML == trs[i].childNodes[3].innerHTML )
				trs[i].className = 'green';
		}
	</script>
</body>
</html>